﻿namespace labFormMDI
{
    partial class fmMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miCreateNewForm = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsCascade = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileHorizontal = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsTileVertical = new System.Windows.Forms.ToolStripMenuItem();
            this.miWindowsArrangeIcons = new System.Windows.Forms.ToolStripMenuItem();
            this.closeActiveForm = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllForms = new System.Windows.Forms.ToolStripMenuItem();
            this.miAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateNewForm,
            this.windowsToolStripMenuItem,
            this.closeActiveForm,
            this.closeAllForms,
            this.miAbout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // miCreateNewForm
            // 
            this.miCreateNewForm.Name = "miCreateNewForm";
            this.miCreateNewForm.Size = new System.Drawing.Size(130, 24);
            this.miCreateNewForm.Text = "CreateNewForm";
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miWindowsCascade,
            this.miWindowsTileHorizontal,
            this.miWindowsTileVertical,
            this.miWindowsArrangeIcons});
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size(84, 24);
            this.windowsToolStripMenuItem.Text = "Windows";
            // 
            // miWindowsCascade
            // 
            this.miWindowsCascade.Name = "miWindowsCascade";
            this.miWindowsCascade.Size = new System.Drawing.Size(264, 26);
            this.miWindowsCascade.Text = "miWindowsCascade";
            // 
            // miWindowsTileHorizontal
            // 
            this.miWindowsTileHorizontal.Name = "miWindowsTileHorizontal";
            this.miWindowsTileHorizontal.Size = new System.Drawing.Size(264, 26);
            this.miWindowsTileHorizontal.Text = "miWindowsTileHorizontal";
            // 
            // miWindowsTileVertical
            // 
            this.miWindowsTileVertical.Name = "miWindowsTileVertical";
            this.miWindowsTileVertical.Size = new System.Drawing.Size(264, 26);
            this.miWindowsTileVertical.Text = "miWindowsTileVertical";
            // 
            // miWindowsArrangeIcons
            // 
            this.miWindowsArrangeIcons.Name = "miWindowsArrangeIcons";
            this.miWindowsArrangeIcons.Size = new System.Drawing.Size(264, 26);
            this.miWindowsArrangeIcons.Text = "miWindowsArrangeIcons";
            // 
            // closeActiveFormToolStripMenuItem
            // 
            this.closeActiveForm.Name = "closeActiveFormToolStripMenuItem";
            this.closeActiveForm.Size = new System.Drawing.Size(134, 24);
            this.closeActiveForm.Text = "CloseActiveForm";
            // 
            // closeAllFormsToolStripMenuItem
            // 
            this.closeAllForms.Name = "closeAllFormsToolStripMenuItem";
            this.closeAllForms.Size = new System.Drawing.Size(117, 24);
            this.closeAllForms.Text = "CloseAllForms";
            // 
            // aboutToolStripMenuItem
            // 
            this.miAbout.Name = "aboutToolStripMenuItem";
            this.miAbout.Size = new System.Drawing.Size(64, 24);
            this.miAbout.Text = "About";
            // 
            // fmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "fmMain";
            this.Text = "labFormMDI";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miCreateNewForm;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miWindowsCascade;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileHorizontal;
        private System.Windows.Forms.ToolStripMenuItem miWindowsTileVertical;
        private System.Windows.Forms.ToolStripMenuItem miWindowsArrangeIcons;
        private System.Windows.Forms.ToolStripMenuItem closeActiveForm;
        private System.Windows.Forms.ToolStripMenuItem closeAllForms;
        private System.Windows.Forms.ToolStripMenuItem miAbout;
    }
}

