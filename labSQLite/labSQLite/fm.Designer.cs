﻿namespace labSQLite
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buNotesAdd = new System.Windows.Forms.Button();
            this.edNotesPriority = new System.Windows.Forms.NumericUpDown();
            this.edNotesCaption = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button4 = new System.Windows.Forms.Button();
            this.buRunOne = new System.Windows.Forms.Button();
            this.buNotesShow = new System.Windows.Forms.Button();
            this.buUsersShow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.edSQL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lvLogs = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // buNotesAdd
            // 
            this.buNotesAdd.Location = new System.Drawing.Point(735, 418);
            this.buNotesAdd.Name = "buNotesAdd";
            this.buNotesAdd.Size = new System.Drawing.Size(149, 30);
            this.buNotesAdd.TabIndex = 23;
            this.buNotesAdd.Text = "Добавить заметку";
            this.buNotesAdd.UseVisualStyleBackColor = true;
            // 
            // edNotesPriority
            // 
            this.edNotesPriority.Location = new System.Drawing.Point(587, 423);
            this.edNotesPriority.Name = "edNotesPriority";
            this.edNotesPriority.Size = new System.Drawing.Size(120, 22);
            this.edNotesPriority.TabIndex = 22;
            // 
            // edNotesCaption
            // 
            this.edNotesCaption.Location = new System.Drawing.Point(272, 423);
            this.edNotesCaption.Name = "edNotesCaption";
            this.edNotesCaption.Size = new System.Drawing.Size(309, 22);
            this.edNotesCaption.TabIndex = 21;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(272, 216);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(612, 199);
            this.dataGridView1.TabIndex = 20;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(735, 181);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(149, 29);
            this.button4.TabIndex = 19;
            this.button4.Text = "Выполнить SQL";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // buRunOne
            // 
            this.buRunOne.Location = new System.Drawing.Point(629, 181);
            this.buRunOne.Name = "buRunOne";
            this.buRunOne.Size = new System.Drawing.Size(100, 29);
            this.buRunOne.TabIndex = 18;
            this.buRunOne.Text = "Выполнить";
            this.buRunOne.UseVisualStyleBackColor = true;
            // 
            // buNotesShow
            // 
            this.buNotesShow.Location = new System.Drawing.Point(405, 181);
            this.buNotesShow.Name = "buNotesShow";
            this.buNotesShow.Size = new System.Drawing.Size(92, 29);
            this.buNotesShow.TabIndex = 17;
            this.buNotesShow.Text = "Заметки";
            this.buNotesShow.UseVisualStyleBackColor = true;
            // 
            // buUsersShow
            // 
            this.buUsersShow.Location = new System.Drawing.Point(272, 181);
            this.buUsersShow.Name = "buUsersShow";
            this.buUsersShow.Size = new System.Drawing.Size(127, 29);
            this.buUsersShow.TabIndex = 16;
            this.buUsersShow.Text = "Пользователи";
            this.buUsersShow.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(426, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 25);
            this.label2.TabIndex = 15;
            this.label2.Text = "SQL  Запрос";
            // 
            // edSQL
            // 
            this.edSQL.Location = new System.Drawing.Point(272, 39);
            this.edSQL.Multiline = true;
            this.edSQL.Name = "edSQL";
            this.edSQL.Size = new System.Drawing.Size(612, 136);
            this.edSQL.TabIndex = 14;
            this.edSQL.Text = "SELECT max(n.priority) From Notes n;";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(11, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 25);
            this.label1.TabIndex = 13;
            this.label1.Text = "Лог запусков приложения";
            // 
            // lvLogs
            // 
            this.lvLogs.HideSelection = false;
            this.lvLogs.Location = new System.Drawing.Point(12, 39);
            this.lvLogs.Name = "lvLogs";
            this.lvLogs.Size = new System.Drawing.Size(254, 406);
            this.lvLogs.TabIndex = 12;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 452);
            this.Controls.Add(this.buNotesAdd);
            this.Controls.Add(this.edNotesPriority);
            this.Controls.Add(this.edNotesCaption);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buRunOne);
            this.Controls.Add(this.buNotesShow);
            this.Controls.Add(this.buUsersShow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edSQL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvLogs);
            this.Name = "fm";
            this.Text = "labSQLite";
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buNotesAdd;
        private System.Windows.Forms.NumericUpDown edNotesPriority;
        private System.Windows.Forms.TextBox edNotesCaption;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buRunOne;
        private System.Windows.Forms.Button buNotesShow;
        private System.Windows.Forms.Button buUsersShow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edSQL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvLogs;
    }
}

