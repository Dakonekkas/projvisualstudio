﻿using labPazzle.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPazzle
{
    public partial class fm : Form
    {
        private Bitmap b;
        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 4;
        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }
        public int CurRow { get; private set; }
        public int CurCol { get; private set; }
        public Bitmap Image { get; private set; }

        private PictureBox[,] pics;
        private Point StartPoint;
        string img = "pic1";

        public fm()
        {
            InitializeComponent();

            

            CreateCells();
            StartLocatoinCells();
            ResizeCells(img);
            ResizeCella();
            DrawCells();
            
            this.Text += "(F1 - Собрать,F2 - Перемешать, F3- Новый размер)";
            this.KeyDown += Fm_KeyDown;
            this.Paint += Fm_Paint;
        }

        private void ResizeCella()
        {
            b = new Bitmap(Width, Height);
            CellWidth = ClientSize.Width / Cols;
            CellHeight = ClientSize.Height / Rows;
        }

        private void PictureBoxAll_MouseUP(object sender, MouseEventArgs e)
        {
            Point x = new Point(CurCol * CellWidth, CurRow * CellHeight);
            Boolean flag = false;
            if (sender is Control a)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Cols; j++)
                    {
                        if (pics[i, j].Location == x)
                        {
                            flag = true;
                            return;
                        }
                    }
                }
                if (!flag)
                {
                    if (CurCol >= 0 && CurRow >= 0 && CurCol < Cols && CurRow < Rows)
                    {
                        a.Location = x;
                        a.SendToBack();
                    }
                    else
                    {
                        a.Location = StartPoint;
                        a.BringToFront();
                    }
                }
            }
        }

        //private void Fm_MouseMove(object sender, MouseEventArgs e)
        //{
            
        //}

        private void DrawCells()
        {
            using (var g = Graphics.FromImage(b))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i <= Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), 0, i * CellHeight, Cols * CellWidth, i * CellHeight);
                }
                for (int j = 0; j <= Cols; j++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), j * CellWidth, 0, j * CellWidth, Rows * CellHeight);
                }
                if (CurCol >= 0 && CurRow >= 0 && CurCol < Cols && CurRow < Rows)
                {
                    g.DrawRectangle(new Pen(Color.Red, 3), CurCol * CellWidth, CurRow * CellHeight, CellWidth, CellHeight);
                }
            }
        }

        private void Fm_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point(0, 0));
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    StartLocatoinCells();
                    break;

                case Keys.F2:
                    RandomCells();
                    break;

                case Keys.F3:
                    StartLocatoinCells();
                    ResizeCells(img);
                    ResizeCella();
                    break;

                case Keys.F4:
                    showChooseModeDialog();
                    break;

                case Keys.F5:
                    img = "pic2";
                    ResizeCells(img);
                    ResizeCella();
                    break;

                case Keys.F6:
                    img = "pic1";
                    ResizeCells(img);
                    ResizeCella();
                    break;
            }
        }

        private void showChooseModeDialog()
        {
            Mode modeDialog = new Mode();

            if (modeDialog.ShowDialog(this) == DialogResult.OK)
            {
                removeOldCalls();
                Cols = modeDialog.getCol();
                Rows = modeDialog.getRow();
                CreateCells();
                StartLocatoinCells();
                ResizeCells(img);
                ResizeCella();
            }

            modeDialog.Dispose();
        }

        private void removeOldCalls()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    this.Controls.Remove(pics[i, j]);

                }
        }

        private void RandomCells()
        {
            Random rnd = new Random();
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(
                        rnd.Next(ClientSize.Width - pics[i, j].Width),
                        rnd.Next(ClientSize.Height - pics[i, j].Height));
                }
        }

        private void ResizeCells(string img)
        {

            Image namepic = Resources.ResourceManager.GetObject(img) as Image;
            Bitmap image = new Bitmap(namepic, new Size(ClientSize.Width, ClientSize.Height));

            int xCellWidth = this.ClientSize.Width / Cols;
            int xCellHeight = this.ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Width = xCellWidth;
                    pics[i, j].Height = xCellHeight;
                    pics[i, j].Image = new Bitmap(xCellWidth, xCellHeight);
                    var g = Graphics.FromImage(pics[i, j].Image);
                    //g.Clear(Color.LightGreen);
                    g.DrawImage(image as Image,
                        new Rectangle(0, 0, pics[i, j].Width, pics[i, j].Height),
                        new Rectangle(j * xCellWidth, i * xCellHeight, xCellWidth, xCellHeight),
                        GraphicsUnit.Pixel);

                    g.Dispose();
                }
        }

        private void StartLocatoinCells()
        {
            int xCellWidth = this.ClientSize.Width / Cols;
            int xCellHeight = this.ClientSize.Height / Rows;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j].Location = new Point(j * xCellWidth, i * xCellHeight);
                }
        }

        private void CreateCells()
        {
            pics = new PictureBox[Rows, Cols];
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Cols; j++)
                {
                    pics[i, j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;
                    pics[i, j].MouseDown += PictureBoxAll_MouseDown;
                    pics[i, j].MouseMove += PictureBoxAll_MouseMove;
                    pics[i, j].MouseUp += PictureBoxAll_MouseUP;
                    this.Controls.Add(pics[i, j]);
                }
        }

        private void PictureBoxAll_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point x = new Point(Cursor.Position.X - StartPoint.X, Cursor.Position.Y - StartPoint.Y);
                if (sender is Control a)
                {
                    a.BringToFront();
                    a.Location = PointToClient(x);
                    CurRow = PointToClient(x).Y / CellHeight;
                    CurCol = PointToClient(x).X / CellWidth;
                    //this.Text = $"({CurRow},{CurCol})";
                    DrawCells();
                    this.CreateGraphics().DrawImage(b, new Point(0, 0));
                }
            }
        }

        private void PictureBoxAll_MouseDown(object sender, MouseEventArgs e)
        {
            StartPoint = new Point(e.X, e.Y);
        }
    }
}
