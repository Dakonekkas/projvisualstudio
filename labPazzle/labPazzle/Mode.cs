﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPazzle
{
    public partial class Mode : Form
    {
        private int curRow;
        private int curCol;
        public Mode()
        {
            InitializeComponent();
            if (curRow != 0 && curCol != 0)
            {
                ChooseRow.Value = curRow;
                ChooseCol.Value = curCol;
            }

            buOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            buCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public int getRow()
        {
            curRow = Convert.ToInt32(ChooseRow.Value);
            return curRow;
        }

        public int getCol()
        {
            curCol = Convert.ToInt32(ChooseCol.Value);
            return curCol;
        }
    }
}
