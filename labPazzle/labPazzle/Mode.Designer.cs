﻿namespace labPazzle
{
    partial class Mode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buOk = new System.Windows.Forms.Button();
            this.buCancel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ChooseCol = new System.Windows.Forms.NumericUpDown();
            this.ChooseRow = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.ChooseCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChooseRow)).BeginInit();
            this.SuspendLayout();
            // 
            // buOk
            // 
            this.buOk.FlatAppearance.BorderSize = 0;
            this.buOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buOk.Location = new System.Drawing.Point(46, 107);
            this.buOk.Name = "buOk";
            this.buOk.Size = new System.Drawing.Size(118, 39);
            this.buOk.TabIndex = 6;
            this.buOk.Text = "Oк";
            this.buOk.UseVisualStyleBackColor = true;
            // 
            // buCancel
            // 
            this.buCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buCancel.FlatAppearance.BorderSize = 0;
            this.buCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buCancel.Location = new System.Drawing.Point(189, 107);
            this.buCancel.Name = "buCancel";
            this.buCancel.Size = new System.Drawing.Size(139, 39);
            this.buCancel.TabIndex = 7;
            this.buCancel.Text = "Отмена";
            this.buCancel.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(41, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 29);
            this.label3.TabIndex = 12;
            this.label3.Text = "Столбцы";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(41, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 29);
            this.label2.TabIndex = 11;
            this.label2.Text = "Строки";
            // 
            // ChooseCol
            // 
            this.ChooseCol.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChooseCol.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChooseCol.Location = new System.Drawing.Point(189, 56);
            this.ChooseCol.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ChooseCol.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.ChooseCol.Name = "ChooseCol";
            this.ChooseCol.Size = new System.Drawing.Size(139, 36);
            this.ChooseCol.TabIndex = 9;
            this.ChooseCol.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // ChooseRow
            // 
            this.ChooseRow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChooseRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChooseRow.Location = new System.Drawing.Point(189, 14);
            this.ChooseRow.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ChooseRow.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.ChooseRow.Name = "ChooseRow";
            this.ChooseRow.Size = new System.Drawing.Size(139, 36);
            this.ChooseRow.TabIndex = 8;
            this.ChooseRow.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // Mode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 155);
            this.Controls.Add(this.buOk);
            this.Controls.Add(this.buCancel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ChooseCol);
            this.Controls.Add(this.ChooseRow);
            this.Name = "Mode";
            this.Text = "Mode";
            ((System.ComponentModel.ISupportInitialize)(this.ChooseCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChooseRow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buOk;
        private System.Windows.Forms.Button buCancel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown ChooseCol;
        private System.Windows.Forms.NumericUpDown ChooseRow;
    }
}