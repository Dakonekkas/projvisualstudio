﻿namespace labGraphicsDrawLine
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pxImage = new System.Windows.Forms.PictureBox();
            this.pxColor1 = new System.Windows.Forms.PictureBox();
            this.pxColor2 = new System.Windows.Forms.PictureBox();
            this.pxColor3 = new System.Windows.Forms.PictureBox();
            this.pxColor4 = new System.Windows.Forms.PictureBox();
            this.trPenWidth = new System.Windows.Forms.TrackBar();
            this.buClear = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buClear);
            this.panel1.Controls.Add(this.trPenWidth);
            this.panel1.Controls.Add(this.pxColor4);
            this.panel1.Controls.Add(this.pxColor3);
            this.panel1.Controls.Add(this.pxColor2);
            this.panel1.Controls.Add(this.pxColor1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 450);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pxImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pxImage.Location = new System.Drawing.Point(251, 0);
            this.pxImage.Name = "pictureBox1";
            this.pxImage.Size = new System.Drawing.Size(549, 450);
            this.pxImage.TabIndex = 1;
            this.pxImage.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pxColor1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pxColor1.Location = new System.Drawing.Point(13, 13);
            this.pxColor1.Name = "pictureBox2";
            this.pxColor1.Size = new System.Drawing.Size(51, 50);
            this.pxColor1.TabIndex = 0;
            this.pxColor1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pxColor2.BackColor = System.Drawing.Color.Green;
            this.pxColor2.Location = new System.Drawing.Point(70, 13);
            this.pxColor2.Name = "pictureBox3";
            this.pxColor2.Size = new System.Drawing.Size(51, 50);
            this.pxColor2.TabIndex = 1;
            this.pxColor2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pxColor3.BackColor = System.Drawing.Color.Navy;
            this.pxColor3.Location = new System.Drawing.Point(127, 13);
            this.pxColor3.Name = "pictureBox4";
            this.pxColor3.Size = new System.Drawing.Size(51, 50);
            this.pxColor3.TabIndex = 2;
            this.pxColor3.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pxColor4.BackColor = System.Drawing.Color.Yellow;
            this.pxColor4.Location = new System.Drawing.Point(184, 13);
            this.pxColor4.Name = "pictureBox5";
            this.pxColor4.Size = new System.Drawing.Size(51, 50);
            this.pxColor4.TabIndex = 3;
            this.pxColor4.TabStop = false;
            // 
            // trackBar1
            // 
            this.trPenWidth.Location = new System.Drawing.Point(13, 83);
            this.trPenWidth.Minimum = 1;
            this.trPenWidth.Name = "trackBar1";
            this.trPenWidth.Size = new System.Drawing.Size(222, 56);
            this.trPenWidth.TabIndex = 4;
            this.trPenWidth.Value = 1;
            // 
            // button1
            // 
            this.buClear.Location = new System.Drawing.Point(13, 145);
            this.buClear.Name = "button1";
            this.buClear.Size = new System.Drawing.Size(222, 34);
            this.buClear.TabIndex = 5;
            this.buClear.Text = "Clear";
            this.buClear.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pxImage);
            this.Controls.Add(this.panel1);
            this.Name = "fm";
            this.Text = "labGraphicsDrawLine";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pxColor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trPenWidth)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buClear;
        private System.Windows.Forms.TrackBar trPenWidth;
        private System.Windows.Forms.PictureBox pxColor4;
        private System.Windows.Forms.PictureBox pxColor3;
        private System.Windows.Forms.PictureBox pxColor2;
        private System.Windows.Forms.PictureBox pxColor1;
        private System.Windows.Forms.PictureBox pxImage;
    }
}

