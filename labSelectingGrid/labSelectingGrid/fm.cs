﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labSelectingGrid
{
    public partial class fm : Form
    {

        private Bitmap b;
        public int Rows { get; private set; } = 3;
        public int Cols { get; private set; } = 4;
        public int CellWidth { get; private set; }
        public int CellHeight { get; private set; }
        public int CurRow { get; private set; }
        public int CurCol { get; private set; }

        public fm()
        {
            InitializeComponent();

            ResizeCells();
            DrawCells();
            this.Paint += Fm_Paint;
            this.Resize += Fm_Resize;
            this.MouseMove += Fm_MouseMove;
        }

        private void Fm_MouseMove(object sender, MouseEventArgs e)
        {
            CurRow =  e.Y / CellHeight;
            CurCol =  e.X / CellWidth;
            this.Text = $"({CurRow},{CurCol})";
            DrawCells();
            this.CreateGraphics().DrawImage(b, new Point(0, 0));
        }

        private void Fm_Resize(object sender, EventArgs e)
        {
            ResizeCells();
            DrawCells();
            this.CreateGraphics().DrawImage(b, new Point(0, 0));
        }

        private void Fm_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point(0, 0));
        }

        private void DrawCells()
        {
            //var g = Graphics.FromImage(b);
            // ...
            //g.Dispose();

            using (var g = Graphics.FromImage(b))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i < Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), 0, i * CellHeight, Cols * CellWidth, i * CellHeight);
                }
                for (int j = 0; j < Cols; j++)
                {
                    g.DrawLine(new Pen(Color.Green, 1), j * CellWidth, 0,  j * CellWidth, Rows * CellHeight);
                }
                g.DrawRectangle(new Pen(Color.Yellow, 3),CurCol * CellWidth, CurRow * CellHeight, CellWidth, CellHeight);
            }

        }

        private void ResizeCells()
        {
            b = new Bitmap(Width, Height);
            CellWidth = ClientSize.Width / Cols;
            CellHeight = ClientSize.Height / Rows;

        }
    }
}
