﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labFileExplorer
{
    public partial class fm : Form
    {
        private string _CurDir;

        public string CurDir 
        {
            get 
            {
                return _CurDir;
            }
            private set 
            {
                _CurDir = value;
                edDir.Text = value;
            }
        }
        public string SelItem { get; private set; }

        public fm()
        {
            InitializeComponent();

            CurDir = Directory.GetCurrentDirectory();
            //CurDir = "D:/";

            //buBack
            //buForward
            buUp.Click += (s, e) => LoadDir(Directory.GetParent(CurDir).ToString());
            edDir.KeyDown += EdDir_KeyDown;
            buDirSelect.Click += BuDirSelect_Click;

            miVievLargeIcon.Click += (s, e) => lv.View = View.LargeIcon;
            miVievSmallIcon.Click += (s, e) => lv.View = View.SmallIcon;
            miVievList.Click += (s, e) => lv.View = View.List;
            meVievDetails.Click += (s, e) => lv.View = View.Details;
            miVievTile.Click += (s, e) => lv.View = View.Tile;

            lv.ItemSelectionChanged += (s, e) => SelItem = Path.Combine(CurDir, e.Item.Text);
            lv.DoubleClick += (s,e) => LoadDir(SelItem);

            //1
            //var c1 = new ColumnHeader();
            //c1.Text = "Имя";
            //c1.Width = 350;
            //lv.Columns.Add(c1);

            //2
            //lv.Columns.Add(new ColumnHeader() { Text = "Имя", Width = 350 });

            //3
            lv.Columns.Add("Имя", 250);
            lv.Columns.Add("Дата изменения", 150);
            lv.Columns.Add("Тип", 100);
            lv.Columns.Add("Размер", 150);

            //Text += " : Drivers=" + string.Join(" ", Directory.GetLogicalDrives());

            LoadDir(CurDir);
        }

        private void BuDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                LoadDir(dialog.SelectedPath);
            }
        }

        private void EdDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadDir(edDir.Text);
            }
        }

        private void LoadDir(string newDir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(newDir);
            lv.BeginUpdate();
            lv.Items.Clear();
            foreach (var item in directoryInfo.GetDirectories())
            {
                //lv.Items.Add(item.Name, 0);

                //var f = new FileInfo(item.FullName);
                lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), "Папка", ""},
                    0 ));
            }
            foreach (var item in directoryInfo.GetFiles())
            {
                //lv.Items.Add(item.Name, 1);

                var f = new FileInfo(item.FullName);
                lv.Items.Add(new ListViewItem(
                    new string[] { item.Name, item.LastWriteTime.ToString(), "Файл", f.Length.ToString() + " байт" },
                    0));
            }
            lv.EndUpdate();
            CurDir = newDir;
        }
    }
}
