﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"D:\Study\projVisualStudio\temp.txt";
            var list = new List<string>
            {
                "Один",
                "Два",
                "Три"
            };

            var xWriteText = string.Join(Environment.NewLine, list);
            File.WriteAllText(path, xWriteText);

            Console.WriteLine(File.Exists(path));

            var xReadText = File.ReadAllText(path);
            Console.WriteLine(xReadText);

            File.Delete(path);

            FileInfo fileInfo = new FileInfo(path);
            Console.WriteLine(fileInfo.Exists);
        }
    }
}
