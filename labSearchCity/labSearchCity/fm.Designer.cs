﻿namespace labSearchCity
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edSearch = new System.Windows.Forms.TextBox();
            this.laCount = new System.Windows.Forms.Label();
            this.edResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.edSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edSearch.Location = new System.Drawing.Point(13, 13);
            this.edSearch.Name = "textBox1";
            this.edSearch.Size = new System.Drawing.Size(488, 22);
            this.edSearch.TabIndex = 0;
            // 
            // label1
            // 
            this.laCount.AutoSize = true;
            this.laCount.Location = new System.Drawing.Point(12, 50);
            this.laCount.Name = "label1";
            this.laCount.Size = new System.Drawing.Size(46, 17);
            this.laCount.TabIndex = 1;
            this.laCount.Text = "label1";
            // 
            // textBox2
            // 
            this.edResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edResult.Location = new System.Drawing.Point(12, 85);
            this.edResult.Multiline = true;
            this.edResult.Name = "textBox2";
            this.edResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.edResult.Size = new System.Drawing.Size(489, 277);
            this.edResult.TabIndex = 2;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 374);
            this.Controls.Add(this.edResult);
            this.Controls.Add(this.laCount);
            this.Controls.Add(this.edSearch);
            this.Name = "fm";
            this.Text = "labSearchCity";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edSearch;
        private System.Windows.Forms.Label laCount;
        private System.Windows.Forms.TextBox edResult;
    }
}

