﻿namespace labFormDialogs
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edOpenFileDialog = new System.Windows.Forms.TextBox();
            this.buOpenFileDialog = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // edOpenFileDialog
            // 
            this.edOpenFileDialog.Location = new System.Drawing.Point(13, 13);
            this.edOpenFileDialog.Name = "edOpenFileDialog";
            this.edOpenFileDialog.Size = new System.Drawing.Size(633, 22);
            this.edOpenFileDialog.TabIndex = 0;
            // 
            // buOpenFileDialog
            // 
            this.buOpenFileDialog.Location = new System.Drawing.Point(652, 13);
            this.buOpenFileDialog.Name = "buOpenFileDialog";
            this.buOpenFileDialog.Size = new System.Drawing.Size(136, 22);
            this.buOpenFileDialog.TabIndex = 1;
            this.buOpenFileDialog.Text = "OpenFileDialog";
            this.buOpenFileDialog.UseVisualStyleBackColor = true;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 107);
            this.Controls.Add(this.buOpenFileDialog);
            this.Controls.Add(this.edOpenFileDialog);
            this.Name = "fm";
            this.Text = "labFormDialogs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edOpenFileDialog;
        private System.Windows.Forms.Button buOpenFileDialog;
    }
}

