﻿namespace Area_51_UFOShooter
{
    partial class UFO
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UFO));
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.laScore = new System.Windows.Forms.Label();
            this.picArmy = new System.Windows.Forms.PictureBox();
            this.picPlayer = new System.Windows.Forms.PictureBox();
            this.picCollumn2 = new System.Windows.Forms.PictureBox();
            this.picCollumn1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picArmy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCollumn2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCollumn1)).BeginInit();
            this.SuspendLayout();
            // 
            // GameTimer
            // 
            this.GameTimer.Enabled = true;
            this.GameTimer.Interval = 20;
            this.GameTimer.Tick += new System.EventHandler(this.MainTimerEvent);
            // 
            // laScore
            // 
            this.laScore.AutoSize = true;
            this.laScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laScore.ForeColor = System.Drawing.Color.White;
            this.laScore.Location = new System.Drawing.Point(12, 432);
            this.laScore.Name = "laScore";
            this.laScore.Size = new System.Drawing.Size(102, 29);
            this.laScore.TabIndex = 4;
            this.laScore.Text = "Счёт: 0";
            // 
            // picArmy
            // 
            this.picArmy.Image = ((System.Drawing.Image)(resources.GetObject("picArmy.Image")));
            this.picArmy.Location = new System.Drawing.Point(907, 306);
            this.picArmy.Name = "picArmy";
            this.picArmy.Size = new System.Drawing.Size(179, 48);
            this.picArmy.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picArmy.TabIndex = 3;
            this.picArmy.TabStop = false;
            // 
            // picPlayer
            // 
            this.picPlayer.Image = global::Area_51_UFOShooter.Properties.Resources.Alien;
            this.picPlayer.Location = new System.Drawing.Point(12, 146);
            this.picPlayer.Name = "picPlayer";
            this.picPlayer.Size = new System.Drawing.Size(200, 43);
            this.picPlayer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picPlayer.TabIndex = 2;
            this.picPlayer.TabStop = false;
            // 
            // picCollumn2
            // 
            this.picCollumn2.Image = global::Area_51_UFOShooter.Properties.Resources.Column;
            this.picCollumn2.Location = new System.Drawing.Point(283, 279);
            this.picCollumn2.Name = "picCollumn2";
            this.picCollumn2.Size = new System.Drawing.Size(64, 201);
            this.picCollumn2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCollumn2.TabIndex = 1;
            this.picCollumn2.TabStop = false;
            this.picCollumn2.Tag = "Column";
            // 
            // picCollumn1
            // 
            this.picCollumn1.BackColor = System.Drawing.SystemColors.Control;
            this.picCollumn1.Image = global::Area_51_UFOShooter.Properties.Resources.Column;
            this.picCollumn1.Location = new System.Drawing.Point(673, -12);
            this.picCollumn1.Name = "picCollumn1";
            this.picCollumn1.Size = new System.Drawing.Size(64, 201);
            this.picCollumn1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCollumn1.TabIndex = 0;
            this.picCollumn1.TabStop = false;
            this.picCollumn1.Tag = "Column";
            // 
            // UFO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ClientSize = new System.Drawing.Size(923, 470);
            this.Controls.Add(this.laScore);
            this.Controls.Add(this.picArmy);
            this.Controls.Add(this.picPlayer);
            this.Controls.Add(this.picCollumn2);
            this.Controls.Add(this.picCollumn1);
            this.Name = "UFO";
            this.Text = "Area 51 UFO Shooter";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Down);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Up);
            ((System.ComponentModel.ISupportInitialize)(this.picArmy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCollumn2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCollumn1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picCollumn1;
        private System.Windows.Forms.PictureBox picCollumn2;
        private System.Windows.Forms.PictureBox picPlayer;
        private System.Windows.Forms.PictureBox picArmy;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.Label laScore;
    }
}

