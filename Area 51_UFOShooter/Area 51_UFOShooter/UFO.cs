﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace Area_51_UFOShooter
{
    public partial class UFO : Form
    {
        bool UP, DOWN, SHOT, GAMEOVER;

        int score = 0;
        int speed = 8;
        int ArmySpeed = 10;

        Random rnd = new Random();

        int PlayerSpeed = 7;
        int index = 0;

        private MediaPlayer mediaPlayer = new MediaPlayer();

        public UFO()
        {
            InitializeComponent();

            mediaPlayer.Open(new Uri(@"D:\Study\projVisualStudio\Area 51_UFOShooter\track.mp3"));
            //mediaPlayer.Play();

        }

        private void MainTimerEvent(object sender, EventArgs e)
        {
            laScore.Text = "Score: " + score;

            if (UP == true && picPlayer.Top > 0)
            {
                picPlayer.Top -= PlayerSpeed;
            }
            if (DOWN == true && picPlayer.Top + picPlayer.Height < this.ClientSize.Height)
            {
                picPlayer.Top += PlayerSpeed;
            }

            picArmy.Left -= ArmySpeed;

            if (picArmy.Left + picArmy.Width < 0)
            {
                ChangeArmy();
            }

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && (string)x.Tag == "Column")
                {
                    x.Left -= speed;

                    if (x.Left < -200)
                    {
                        x.Left = 1000;
                    }

                    if (picPlayer.Bounds.IntersectsWith(x.Bounds))
                    {
                        GameOver();
                        //mediaPlayer.Stop();
                    }

                 }
                    if (x is PictureBox && (string)x.Tag == "laser")
                    {
                        x.Left += 25;

                        if (x.Left > 900)
                        {
                            ToRemoveForeignObjects(((PictureBox)x));
                        }

                        if (picArmy.Bounds.IntersectsWith(x.Bounds))
                        {
                        ToRemoveForeignObjects(((PictureBox)x));
                        score += 1;
                        ChangeArmy();
                        }

                    }                 
                
            }

            if (picPlayer.Bounds.IntersectsWith(picArmy.Bounds))
            {
                GameOver();
                //mediaPlayer.Stop();

            }

            if (score > 10)
            {
                speed = 12;
                ArmySpeed = 18;
            }

        }

        private void Down(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                UP = true;
            }
            if (e.KeyCode == Keys.Down)
            {
                DOWN = true;
            }
            if (e.KeyCode == Keys.Space && SHOT == false)
            {
                MakeBullet();
                SHOT = true;

            }

        }

        private void Up(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                UP = false;
            }
            if (e.KeyCode == Keys.Down)
            {
                DOWN = false;
            }
            if (SHOT == true)
            {
                SHOT = false;
            }

            if (e.KeyCode == Keys.Enter && GAMEOVER == true)
            {
                GameRestart();
            }
        }

        private void GameRestart()
        {
            UP = false;
            DOWN = false;
            SHOT = false;
            GAMEOVER = false;
            score = 0;
            speed = 8;
            ArmySpeed = 10;

            laScore.Text = "Score: " + score;

            ChangeArmy();

            picPlayer.Top = 146;
            picCollumn1.Left = 635;
            picCollumn2.Left = 323;

            GameTimer.Start();
            //mediaPlayer.Play();
        }

        private void GameOver()
        {
            GameTimer.Stop();
            laScore.Text = "Score: " + score + " Вы проиграли нажмите Enter чтобы начать заного!";
            GAMEOVER = true;
        }

        private void ToRemoveForeignObjects(PictureBox laser)
        {
            this.Controls.Remove(laser);
            laser.Dispose();
        }

        private void MakeBullet()
        {
            PictureBox laser = new PictureBox();
            laser.BackColor = System.Drawing.Color.Red;
            laser.Height = 5;
            laser.Width = 50;

            laser.Left = picPlayer.Left + picPlayer.Width;
            laser.Top = picPlayer.Top + picPlayer.Height / 2;

            laser.Tag = "laser";

            this.Controls.Add(laser);


        }

        private void ChangeArmy()
        {
            if (index > 3)
            {
                index = 1;
            }
            else
            {
                index += 1;
            }

            switch (index)
            {
                case 1:
                    picArmy.Image = Properties.Resources.Copter;
                    break;
                case 2:
                    picArmy.Image = Properties.Resources.Copter2;
                    break;
                case 3:
                    picArmy.Image = Properties.Resources.Copter3;
                    break;
            }

            picArmy.Left = 1000;
            picArmy.Top = rnd.Next(20, this.ClientSize.Height - picArmy.Height);
        }
    }
}
