﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labCreateControlOnMouse
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();
            this.MouseDown += Fm_MouseDown;
        }

        private void Fm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) 
            {
                var x = new Label();
                x.Location = e.Location;
                //x.Text = string.Format("{0},{1}", e.X, e.Y);
                x.Text = $"{e.X},{e.Y}";
                x.Font = new Font("Microsoft Sans Serif", 15F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
                x.ForeColor = Color.Red;
                x.AutoSize = true;
                this.Controls.Add(x);
            }
            if (e.Button == MouseButtons.Right)
            {
                var rnd = new Random();
                for (int i = 0; i < 10; i++)
                {
                    var x = new Label();
                    x.Location = new Point(rnd.Next(Size.Width), rnd.Next(Size.Height));
                    x.Text = $"{x.Location.X},{x.Location.Y}";
                    x.Font = new Font("Microsoft Sans Serif", 15F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
                    x.ForeColor = Color.Black;
                    x.AutoSize = true;
                    this.Controls.Add(x);
                }
            }
        }
    }
}
