﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labStriameWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"D:\Study\projVisualStudio\tempStream.txt";

            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                streamWriter.WriteLine("1111111111");
                streamWriter.WriteLine("22222222");
                streamWriter.WriteLine("3333333");
                Console.WriteLine("Файл создан и сохранен!");
            }

            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine("44");
                Console.WriteLine("Файл дописан и сохранен!");
            }

            using (StreamReader streamReader = new StreamReader(path))
            {
                Console.WriteLine("---начало файла---");
                Console.WriteLine(streamReader.ReadToEnd());
                Console.WriteLine("---конец файла---");
                Console.WriteLine("Файл считан полностью!");
            }

            using (StreamReader streamReader = new StreamReader(path))
            {
                string line;
                int n = 1;
                while((line = streamReader.ReadLine()) != null)
                {
                    Console.WriteLine($"Сторока {n++} = [{line}]");
                }
                Console.WriteLine("Файл считан построчно!");
            }

            var a = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            using (StreamWriter streamWriter = new StreamWriter(path, true))
            {
                foreach (var item in a)
                {
                    streamWriter.Write(item);
                }                
                Console.WriteLine("Файл дописан и сохранен!");
            }

        }
    }
}
