﻿namespace LabRoadEditor
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buTiles = new System.Windows.Forms.Button();
            this.paTiles = new System.Windows.Forms.Panel();
            this.checkMoveBtn = new System.Windows.Forms.CheckBox();
            this.paSetting = new System.Windows.Forms.Panel();
            this.buPouringMap = new System.Windows.Forms.Button();
            this.buCursor = new System.Windows.Forms.Button();
            this.buClearOne = new System.Windows.Forms.Button();
            this.buClearAll = new System.Windows.Forms.Button();
            this.buDownload = new System.Windows.Forms.Button();
            this.buUpload = new System.Windows.Forms.Button();
            this.laZoomPercent = new System.Windows.Forms.Label();
            this.map = new System.Windows.Forms.PictureBox();
            this.piChooseTile = new System.Windows.Forms.PictureBox();
            this.paSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.map)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.piChooseTile)).BeginInit();
            this.SuspendLayout();
            // 
            // buTiles
            // 
            this.buTiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buTiles.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buTiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buTiles.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buTiles.ForeColor = System.Drawing.Color.Black;
            this.buTiles.Location = new System.Drawing.Point(1059, 12);
            this.buTiles.Name = "buTiles";
            this.buTiles.Size = new System.Drawing.Size(419, 52);
            this.buTiles.TabIndex = 2;
            this.buTiles.Text = "▼    Тайлы";
            this.buTiles.UseVisualStyleBackColor = false;
            // 
            // paTiles
            // 
            this.paTiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paTiles.AutoScroll = true;
            this.paTiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paTiles.Location = new System.Drawing.Point(1059, 70);
            this.paTiles.Name = "paTiles";
            this.paTiles.Size = new System.Drawing.Size(419, 604);
            this.paTiles.TabIndex = 3;
            // 
            // checkMoveBtn
            // 
            this.checkMoveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkMoveBtn.Font = new System.Drawing.Font("Comic Sans MS", 12.8F);
            this.checkMoveBtn.Location = new System.Drawing.Point(9, 54);
            this.checkMoveBtn.Name = "checkMoveBtn";
            this.checkMoveBtn.Size = new System.Drawing.Size(224, 75);
            this.checkMoveBtn.TabIndex = 4;
            this.checkMoveBtn.Text = "Выкл. перетаскивание";
            this.checkMoveBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkMoveBtn.UseVisualStyleBackColor = true;
            // 
            // paSetting
            // 
            this.paSetting.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.paSetting.BackColor = System.Drawing.Color.White;
            this.paSetting.Controls.Add(this.buPouringMap);
            this.paSetting.Controls.Add(this.buCursor);
            this.paSetting.Controls.Add(this.buClearOne);
            this.paSetting.Controls.Add(this.buClearAll);
            this.paSetting.Controls.Add(this.buDownload);
            this.paSetting.Controls.Add(this.buUpload);
            this.paSetting.Controls.Add(this.laZoomPercent);
            this.paSetting.Controls.Add(this.checkMoveBtn);
            this.paSetting.Location = new System.Drawing.Point(0, 0);
            this.paSetting.Name = "paSetting";
            this.paSetting.Size = new System.Drawing.Size(236, 686);
            this.paSetting.TabIndex = 4;
            // 
            // buPouringMap
            // 
            this.buPouringMap.BackgroundImage = global::LabRoadEditor.Properties.Resources.pintar__1_;
            this.buPouringMap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buPouringMap.FlatAppearance.BorderSize = 0;
            this.buPouringMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buPouringMap.Location = new System.Drawing.Point(119, 217);
            this.buPouringMap.Name = "buPouringMap";
            this.buPouringMap.Size = new System.Drawing.Size(105, 67);
            this.buPouringMap.TabIndex = 11;
            this.buPouringMap.UseVisualStyleBackColor = true;
            // 
            // buCursor
            // 
            this.buCursor.BackgroundImage = global::LabRoadEditor.Properties.Resources.cursor;
            this.buCursor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buCursor.FlatAppearance.BorderSize = 0;
            this.buCursor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buCursor.Location = new System.Drawing.Point(12, 307);
            this.buCursor.Name = "buCursor";
            this.buCursor.Size = new System.Drawing.Size(102, 67);
            this.buCursor.TabIndex = 10;
            this.buCursor.UseVisualStyleBackColor = true;
            // 
            // buClearOne
            // 
            this.buClearOne.BackgroundImage = global::LabRoadEditor.Properties.Resources.cancelar;
            this.buClearOne.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buClearOne.FlatAppearance.BorderSize = 0;
            this.buClearOne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buClearOne.Location = new System.Drawing.Point(119, 307);
            this.buClearOne.Name = "buClearOne";
            this.buClearOne.Size = new System.Drawing.Size(105, 67);
            this.buClearOne.TabIndex = 9;
            this.buClearOne.UseVisualStyleBackColor = true;
            // 
            // buClearAll
            // 
            this.buClearAll.BackgroundImage = global::LabRoadEditor.Properties.Resources.compartimiento;
            this.buClearAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buClearAll.FlatAppearance.BorderSize = 0;
            this.buClearAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buClearAll.Location = new System.Drawing.Point(12, 217);
            this.buClearAll.Name = "buClearAll";
            this.buClearAll.Size = new System.Drawing.Size(102, 67);
            this.buClearAll.TabIndex = 8;
            this.buClearAll.UseVisualStyleBackColor = true;
            // 
            // buDownload
            // 
            this.buDownload.BackgroundImage = global::LabRoadEditor.Properties.Resources.salvar;
            this.buDownload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buDownload.FlatAppearance.BorderSize = 0;
            this.buDownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buDownload.Location = new System.Drawing.Point(119, 135);
            this.buDownload.Name = "buDownload";
            this.buDownload.Size = new System.Drawing.Size(105, 67);
            this.buDownload.TabIndex = 7;
            this.buDownload.UseVisualStyleBackColor = true;
            // 
            // buUpload
            // 
            this.buUpload.BackgroundImage = global::LabRoadEditor.Properties.Resources.salvar2;
            this.buUpload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buUpload.FlatAppearance.BorderSize = 0;
            this.buUpload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buUpload.Location = new System.Drawing.Point(12, 135);
            this.buUpload.Name = "buUpload";
            this.buUpload.Size = new System.Drawing.Size(102, 67);
            this.buUpload.TabIndex = 6;
            this.buUpload.UseVisualStyleBackColor = true;
            // 
            // laZoomPercent
            // 
            this.laZoomPercent.Font = new System.Drawing.Font("Comic Sans MS", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laZoomPercent.Location = new System.Drawing.Point(12, 10);
            this.laZoomPercent.Name = "laZoomPercent";
            this.laZoomPercent.Size = new System.Drawing.Size(215, 41);
            this.laZoomPercent.TabIndex = 5;
            this.laZoomPercent.Text = "Zoom: 50%";
            this.laZoomPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // map
            // 
            this.map.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.map.Dock = System.Windows.Forms.DockStyle.Fill;
            this.map.Location = new System.Drawing.Point(0, 0);
            this.map.Name = "map";
            this.map.Size = new System.Drawing.Size(1490, 686);
            this.map.TabIndex = 1;
            this.map.TabStop = false;
            // 
            // piChooseTile
            // 
            this.piChooseTile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.piChooseTile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.piChooseTile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.piChooseTile.Location = new System.Drawing.Point(1001, 12);
            this.piChooseTile.Name = "piChooseTile";
            this.piChooseTile.Size = new System.Drawing.Size(52, 52);
            this.piChooseTile.TabIndex = 12;
            this.piChooseTile.TabStop = false;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1490, 686);
            this.Controls.Add(this.paSetting);
            this.Controls.Add(this.piChooseTile);
            this.Controls.Add(this.paTiles);
            this.Controls.Add(this.buTiles);
            this.Controls.Add(this.map);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Name = "fm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "LabRoadEditor";
            this.paSetting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.map)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.piChooseTile)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox map;
        private System.Windows.Forms.Button buTiles;
        private System.Windows.Forms.Panel paTiles;
        private System.Windows.Forms.CheckBox checkMoveBtn;
        private System.Windows.Forms.Panel paSetting;
        private System.Windows.Forms.Label laZoomPercent;
        private System.Windows.Forms.Button buDownload;
        private System.Windows.Forms.Button buUpload;
        private System.Windows.Forms.Button buClearAll;
        private System.Windows.Forms.Button buClearOne;
        private System.Windows.Forms.Button buCursor;
        private System.Windows.Forms.Button buPouringMap;
        private System.Windows.Forms.PictureBox piChooseTile;
    }
}

