﻿namespace LabRoadEditor
{
    partial class ModeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChooseRow = new System.Windows.Forms.NumericUpDown();
            this.ChooseCol = new System.Windows.Forms.NumericUpDown();
            this.buOk = new System.Windows.Forms.Button();
            this.buCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ChooseRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChooseCol)).BeginInit();
            this.SuspendLayout();
            // 
            // ChooseRow
            // 
            this.ChooseRow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChooseRow.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ChooseRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChooseRow.Location = new System.Drawing.Point(12, 68);
            this.ChooseRow.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.ChooseRow.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ChooseRow.Name = "ChooseRow";
            this.ChooseRow.Size = new System.Drawing.Size(370, 49);
            this.ChooseRow.TabIndex = 0;
            this.ChooseRow.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // ChooseCol
            // 
            this.ChooseCol.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChooseCol.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ChooseCol.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F);
            this.ChooseCol.Location = new System.Drawing.Point(12, 163);
            this.ChooseCol.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.ChooseCol.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ChooseCol.Name = "ChooseCol";
            this.ChooseCol.Size = new System.Drawing.Size(370, 49);
            this.ChooseCol.TabIndex = 1;
            this.ChooseCol.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // buOk
            // 
            this.buOk.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buOk.BackColor = System.Drawing.Color.DodgerBlue;
            this.buOk.FlatAppearance.BorderSize = 0;
            this.buOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buOk.Location = new System.Drawing.Point(12, 247);
            this.buOk.Name = "buOk";
            this.buOk.Size = new System.Drawing.Size(182, 74);
            this.buOk.TabIndex = 2;
            this.buOk.Text = "ОК";
            this.buOk.UseVisualStyleBackColor = false;
            // 
            // buCancel
            // 
            this.buCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.buCancel.FlatAppearance.BorderSize = 0;
            this.buCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buCancel.Location = new System.Drawing.Point(200, 247);
            this.buCancel.Name = "buCancel";
            this.buCancel.Size = new System.Drawing.Size(182, 74);
            this.buCancel.TabIndex = 3;
            this.buCancel.Text = "ОТМЕНА";
            this.buCancel.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Строки";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Колонки";
            // 
            // ModeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(394, 346);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buCancel);
            this.Controls.Add(this.buOk);
            this.Controls.Add(this.ChooseCol);
            this.Controls.Add(this.ChooseRow);
            this.MaximumSize = new System.Drawing.Size(412, 393);
            this.MinimumSize = new System.Drawing.Size(412, 393);
            this.Name = "ModeDialog";
            this.Text = "ModeDialog";
            ((System.ComponentModel.ISupportInitialize)(this.ChooseRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChooseCol)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown ChooseRow;
        private System.Windows.Forms.NumericUpDown ChooseCol;
        private System.Windows.Forms.Button buOk;
        private System.Windows.Forms.Button buCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}