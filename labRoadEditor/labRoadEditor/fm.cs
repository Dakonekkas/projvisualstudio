﻿using LabRoadEditor.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.OctavalentExtensions;

namespace LabRoadEditor
{
    public partial class fm : Form
    {
        public int row { get; private set; } = 20;
        public int col { get; private set; } = 20;
        private int sizeCellsMap = 70;
        List<Point> localTile;
        private Bitmap bmpPaTiles;
        private int cX;
        private int cY;
        private int curRow;
        private int curCol;
        private bool isDown = false;
        private Point startPoint; 
        private Point CurPoint;
        private int x = -1;
        private int y = -1;

        private PictureBox[,] pics;
        private int[,] saveMap;

        private int rowPic = 16;
        private int colPic = 3;
        private int differenceX;
        private int differenceY;
        private PictureBox chooseTile = null;
        private int destWidth;
        private int destHeight;
        private int zoomPercent = 50;
        private bool flagClear = false;
        private bool invoke = false;
        private PictureBox effectChoosePictureBox = null;

        public fm()
        {
            InitializeComponent();

            startStateWindwo();
            buildSaveMapList();

            bmpPaTiles = new Bitmap(row * sizeCellsMap, col * sizeCellsMap);
            Thread reDrawMap12 = new Thread(new ThreadStart(DrawCellsOnMap));
            reDrawMap12.Start();
            CreateCells();
            StartLocationCells();
            ResizeCells(Resources.pic1 as Image);

            buTiles.Click += showPaTiles;
            buUpload.Click += BuUpload_Click;
            buDownload.Click += BuDownload_Click;
            buClearAll.Click += BuClearAll_Click;
            buClearOne.Click += BuClearOne_Click;
            buCursor.Click += BuCursor_Click;
            buPouringMap.Click += BuPouringMap_Click;

            map.MouseMove += Map_MouseMove;
            map.MouseDown += Map_MouseDown;
            map.Paint += Form1_Paint;

            MouseWheel += Fm_MouseWheel;
            KeyDown += Fm_KeyDown;
        }

        private void BuPouringMap_Click(object sender, EventArgs e)
        {
            checkMoveBtn.Checked = true;

            if (chooseTile == null)
            {
                MessageBox.Show("Выбери тайл!!!");
                return;
            }

            Thread reDrawMap12 = new Thread(new ThreadStart(renderingPaintAll));
            reDrawMap12.Start();
        }

        private void BuCursor_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.Arrow;
            flagClear = false;
        }

        private void BuClearOne_Click(object sender, EventArgs e)
        {
            piChooseTile.BackgroundImage = null;
            chooseTile = null;
            piChooseTile.BackColor = Color.FromArgb(53, 129, 227);
            Cursor = Cursors.Cross;
            flagClear = true;
        }

        private void BuClearAll_Click(object sender, EventArgs e)
        {
            DrawCellsOnMap();
            Array.Clear(saveMap, 0, saveMap.Length);
            buildSaveMapList();
            map.Invalidate();
        }

        private void BuDownload_Click(object sender, EventArgs e)
        {
            downloadMapFromFile();
        }

        private void BuUpload_Click(object sender, EventArgs e)
        {
            seveMapOnFile();
        }

        private void Fm_MouseWheel(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Delta < 0) 
                {
                    if (zoomPercent >= 2)
                    {
                        // to do
                        // оптимизировать 
                        zoomPercent -= 2;
                        laZoomPercent.Text = $"Zoom: {zoomPercent}%";
                        cX = ((int)(bmpPaTiles.Width - 50) / col) * row / row;
                        cY = ((int)(bmpPaTiles.Height - 50) / row) * col / col;
                        bmpPaTiles = BitmapExtensions.ScaleByPercent(bmpPaTiles, 50);
                        Thread reDrawMap12 = new Thread(new ThreadStart(rendering));
                        reDrawMap12.Start();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                    }
                }
                else
                {
                    if(zoomPercent <= 98)
                    {
                        // to do
                        // оптимизировать 
                        zoomPercent += 2;
                        laZoomPercent.Text = $"Zoom: {zoomPercent}%";
                        cX = ((int)(bmpPaTiles.Width + 50) / col) * row / row;
                        cY = ((int)(bmpPaTiles.Height + 50) / row) * col / col;
                        bmpPaTiles = BitmapExtensions.ScaleByPercent(bmpPaTiles, -50);
                        Thread reDrawMap12 = new Thread(new ThreadStart(rendering));
                        reDrawMap12.Start();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                    }
                }
                
            }
            catch{}
        }

        private void showChooseModeDialog()
        {
            ModeDialog modeDialog = new ModeDialog(row, col);

            if (modeDialog.ShowDialog(this) == DialogResult.OK)
            {
                col = modeDialog.getCol();
                row = modeDialog.getRow();
                Array.Clear(saveMap, 0, saveMap.Length);
                buildSaveMapList();
                invoke = true;
                zoomPercent = 50;
                bmpPaTiles = new Bitmap(row * sizeCellsMap, col * sizeCellsMap);
                
                Thread reDrawMap12 = new Thread(new ThreadStart(DrawCellsOnMap));
                reDrawMap12.Start();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
           
            modeDialog.Dispose();
        }


        private void startStateWindwo()
        {
            Text = " F1 - [Изменить размер] ";
            localTile = new List<Point>();

            WindowState = FormWindowState.Maximized;

            cX = row * sizeCellsMap / col;
            cY = col * sizeCellsMap / row;

            buTiles.BackColor = Color.FromArgb(136, 175, 235);
            paTiles.BackColor = Color.FromArgb(136, 175, 235);
            map.BackColor = Color.FromArgb(53, 129, 227);
            piChooseTile.BackColor = Color.FromArgb(53, 129, 227);
        }


        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    showChooseModeDialog();
                    break;
            }
        }

        private void downloadMapFromFile()
        {
            OpenFileDialog open_dialog = new OpenFileDialog(); 
            open_dialog.Filter = "Text Files | *.txt";

           
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    string[] temp = null;
                    string[] lines = File.ReadAllLines(open_dialog.FileName);
                    int[,] num = new int[lines.Length, lines[0].Split(' ').Length];
                    for (int i = 0; i < lines.Length; i++)
                    {
                        temp = lines[i].Split(' ');
                        for (int j = 0; j < temp.Length; j++)
                            num[i, j] = Int32.Parse(temp[j]);
                    }

                    if(col != temp.Length || row != lines.Length)
                    {
                        MessageBox.Show($"Размер загружаемой карты не совпадает с размером вашей карты!\n" +
                            $"Ее размер {temp.Length} строк, {temp.Length} колонок! Измените размер.");
                        showChooseModeDialog();
                        return;
                    }


                    saveMap = num;

                    try
                    {
                        Thread LoadingThread = new Thread(new ThreadStart(ArrayLoadingToMap));
                        LoadingThread.Start();
                    }
                    catch
                    {
                        MessageBox.Show("Ошибка. Проверте загружаемую карту, возможно она не того размера....");
                    }
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Что-то пошло не так...",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ArrayLoadingToMap()
        {
            ComparisonDoubleArray();
        }

        private void rendering()
        {
            try
            {
                Thread reDrawMap12 = new Thread(new ThreadStart(DrawCells));
                reDrawMap12.Start();
                Thread reDrawMap2 = new Thread(new ThreadStart(ComparisonDoubleArray));
                reDrawMap2.Start();
            }
            catch {}
        }

        private void renderingPaintAll()
        {
            try
            {
                PainAllCalls();
                ComparisonDoubleArray();
            }
            catch { }
        }
        
        private void ComparisonDoubleArray()
        {
            try
            {
                using (var g = Graphics.FromImage(bmpPaTiles))
                {
                    int n = 0;
                    int k = 0;
                    while (n < rowPic)
                    {
                        for (int i = 0; i < row; i++)
                            for (int j = 0; j < col; j++)
                            {
                                if (n != 16)
                                {
                                    if (saveMap[i, j] == Convert.ToInt32(pics[n, k].Tag))
                                    {
                                        g.DrawImage(pics[n, k].Image, j * (cY - differenceY), i * (cX - differenceX), cY - differenceY, cX - differenceX);
                                    }

                                    if (i == row - 1 && j == col - 1)
                                    {
                                        if (k < colPic - 1)
                                            k++;
                                        else
                                        {
                                            n++; k = 0;
                                        }
                                    }
                                }
                                else
                                {
                                    return;
                                }
                            }
                    }
                    this.Invoke((MethodInvoker)delegate () { map.Refresh(); });
                }
            }
            catch {}
        }

        private void seveMapOnFile()
        {
            SaveFileDialog savedialog = new SaveFileDialog();
            savedialog.Title = "Сохранить картинку как...";
            //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
            savedialog.OverwritePrompt = true;
            //отображать ли предупреждение, если пользователь указывает несуществующий путь
            savedialog.CheckPathExists = true;
            //список форматов файла, отображаемый в поле "Тип файла"
            savedialog.Filter = "Text Files | *.txt";
            //отображается ли кнопка "Справка" в диалоговом окне
            savedialog.ShowHelp = true;
            if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
            {
                using (StreamWriter stream = new StreamWriter(savedialog.FileName))
                {
                    try
                    {
                        for (int i = 0; i < row; i++)
                        {
                            for (int j = 0; j < col; j++)
                                if(j != col - 1)
                                    stream.Write("{0} ", saveMap[i, j]);
                                else
                                    stream.Write("{0}", saveMap[i, j]);
                            stream.WriteLine();
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void buildSaveMapList()
        {
            saveMap = new int[row, col];

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    saveMap[i, j] = 0;
        }

        private void ResizeCells(Image pz)
        {
            try
            {
                CropTile crop = new CropTile();
                Bitmap bmpPaTiles = new Bitmap(pz, new Size(pz.Width, pz.Height));

                int index = 0;
                for (int i = 0; i < rowPic; i++)
                    for (int j = 0; j < colPic; j++)
                    {
                        pics[i, j].Image = new Bitmap(pics[i, j].Width, pics[i, j].Height);
                        var graf = Graphics.FromImage(pics[i, j].Image);

                        graf.DrawImage(pz,
                           new Rectangle(0, 0, pics[i, j].Width, pics[i, j].Height),
                           crop.tileList[index],
                           GraphicsUnit.Pixel);
                        
                        index += 1;
                        graf.Dispose();
                    }
            }
            catch (Exception){}
        }

        private void CreateCells()
        {
            int tagCount = 1;
            pics = new PictureBox[rowPic, colPic];
            for (int i = 0; i < rowPic; i++)
                for (int j = 0; j < colPic; j++)
                {
                    pics[i, j] = new PictureBox();
                    pics[i, j].BorderStyle = BorderStyle.FixedSingle;
                    pics[i, j].Width = sizeCellsMap;
                    pics[i, j].Height = sizeCellsMap;
                    pics[i, j].Tag = tagCount;
                    pics[i, j].MouseDown += PictureBoxAll_MouseDown;
                    pics[i, j].MouseMove += PictureBoxAll_MouseMove;
                    pics[i, j].MouseUp += Fm_MouseUp;
                    paTiles.Controls.Add(pics[i, j]);
                    tagCount++;
                }
        }

        private void StartLocationCells()
        {
            int pX = paTiles.Width / colPic;
            int pY = paTiles.Height / rowPic;
            int inn = 0;
            for (int i = 0; i < rowPic; i++)
                for (int j = 0; j < colPic; j++)
                {
                    pics[i, j].Location = new Point(j * pX + 5, i * pY * rowPic / 4 +  20);
                    localTile.Add(pics[i, j].Location);
                    inn++;
                }
        }

        private void PictureBoxAll_MouseMove(object sender, MouseEventArgs e)
        {
            if(checkMoveBtn.Checked == false) { 
                if (e.Button == MouseButtons.Left && isDown)
                    if (sender is Control a)
                    {
                        curCol = (Cursor.Position.X - CurPoint.X ) / (cX - differenceX);
                        curRow = (Cursor.Position.Y - CurPoint.Y ) / (cY - differenceY); 


                        paTiles.Visible = false;

                        a.BringToFront();
                        a.Parent = map;
                        Point x = new Point(Cursor.Position.X - startPoint.X, Cursor.Position.Y - startPoint.Y);
                        var XY = PointToClient(x);
                        a.Location = XY;

                        map.Refresh();
                    }
            }
        }


        private void PictureBoxAll_MouseDown(object sender, MouseEventArgs e)
        {
            isDown = true;

            startPoint = new Point(e.X, e.Y);
            Graphics g = Graphics.FromImage(bmpPaTiles);
            x = e.X;
            y = e.Y;
            if (sender is PictureBox a)
            {
                //if (checkMoveBtn.Checked == true)
                //{
                    chooseTile = new PictureBox
                    {
                        Width = sizeCellsMap,
                        Height = sizeCellsMap,
                        Tag = a.Tag,
                        Image = a.Image
                    };
                //}
                
                piChooseTile.BackgroundImage = a.Image;
            }
        }

        private void Fm_MouseUp(object sender, MouseEventArgs e)
        {
            if(sender is PictureBox a && checkMoveBtn.Checked == false)
            {
                respawnTile(a);
                AutoDebugging(sender);
            }

            paTiles.Visible = true;
            isDown = false;
            x = -1;
            y = -1;
        }

        private void AutoDebugging(object sender)
        {
            if (sender is Control a && sender is PictureBox pic)
            {
                try
                {
                    using (var g = Graphics.FromImage(bmpPaTiles))
                    {
                        saveMap[curRow, curCol] = Convert.ToInt32(pic.Tag);
                        g.DrawImage(pic.Image, curCol * (cX - differenceX), curRow * (cY - differenceY), cX - differenceX, cY - differenceY);
                        map.Refresh();
                        this.map.Controls.Remove(pic);
                    }
                }
                catch
                {
                  
                }
            }
        }

        private void respawnTile(PictureBox a)
        {
            Point local = new Point();
            local = localTile[Convert.ToInt32(a.Tag) - 1];
            var copyBtn = new PictureBox
            {
                Location = new Point(local.X, local.Y - paTiles.VerticalScroll.Value),
                Width = sizeCellsMap,
                Height = sizeCellsMap,
                Tag = a.Tag,
                Image = a.Image
            };

            copyBtn.BorderStyle = BorderStyle.FixedSingle;
            copyBtn.MouseDown += PictureBoxAll_MouseDown;
            copyBtn.MouseMove += PictureBoxAll_MouseMove;
            copyBtn.MouseUp += Fm_MouseUp;
            paTiles.Controls.Add(copyBtn);
        }

        private void showPaTiles(object sender, EventArgs e)
        {
            switch (paTiles.Visible) {
                case true:
                    buTiles.Text = "▲    Тайлы";
                    paTiles.Visible = false;
                    break;
                case false:
                    buTiles.Text = "▼    Тайлы";
                    paTiles.Visible = true;
                    break;
            }
        }

        private void Map_MouseMove(object sender, MouseEventArgs e)
        {
            if ((cX - differenceX) != 0 && (cY - differenceY) != 0)
            {
                curCol = (e.X - CurPoint.X) / (cX - differenceX);
                curRow = (e.Y - CurPoint.Y) / (cY - differenceY);
            }

            if (e.Button == MouseButtons.Right)
            {
                if (map.Capture)
                {
                    CurPoint.X += e.X - startPoint.X;
                    CurPoint.Y += e.Y - startPoint.Y;

                    startPoint = e.Location;
                    map.Refresh();
                }
            }
            if (e.Button == MouseButtons.Left)
            {
                PaintTileNotMoveTiles(e);
                clearCells();
            }
        }

        private void PaintTileNotMoveTiles(MouseEventArgs e)
        {
            try
            {
                if (checkMoveBtn.Checked == true && e.Button == MouseButtons.Left
                  && chooseTile != null && !flagClear)
                {
                    using (var g = Graphics.FromImage(bmpPaTiles))
                    {
                        saveMap[curRow, curCol] = Convert.ToInt32(chooseTile.Tag);
                        g.DrawImage(chooseTile.Image, curCol * (cX - differenceX), curRow * (cY - differenceY), cX - differenceX, cY - differenceY);
                        map.Refresh();
                    }
                }
            }
            catch { }
           
        }

        private void Map_MouseDown(object sender, MouseEventArgs e)
        {
            isDown = true;
            startPoint = e.Location;
            try
            {
                if(e.Button == MouseButtons.Left)
                {
                    PaintTileNotMoveTiles(e);
                    clearCells();
                }
            }
            catch
            {
               
            }
        }

        private void clearCells()
        {
            if (flagClear)
            {
                using (var g = Graphics.FromImage(bmpPaTiles))
                {
                    saveMap[curRow, curCol] = 0;
                    g.FillRectangle(new SolidBrush(Color.FromArgb(53, 129, 227)), curCol * (cX - differenceX), curRow * (cY - differenceY), cX - differenceX, cY - differenceY);
                    g.DrawRectangle(new Pen(Color.White, 2), curCol * (cX - differenceX), curRow * (cY - differenceY), cX - differenceX, cY - differenceY);
                    map.Refresh();
                }
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                e.Graphics.DrawImage(bmpPaTiles, CurPoint);
            }
            catch { }
        }

        private void DrawCellsOnMap()
        {
            DrawCells();
        }

        private void DrawCells()
        {
            try
            {
                using (var g = Graphics.FromImage(bmpPaTiles))
                {
                    differenceX = 0;
                    differenceY = 0;

                    if (cX > cY)
                        differenceX = cX - cY;
                    else
                        differenceY = cY - cX;

                    g.Clear(Color.FromArgb(54, 130, 229));
                    for (int i = 0; i <= row; i++)
                        for (int j = 0; j <= col; j++)
                        {
                            g.DrawLine(new Pen(Color.White, 2), 0, i * (cX - differenceX), col * (cX - differenceX), i * (cY - differenceY));
                            g.DrawLine(new Pen(Color.White, 2), j * (cX - differenceX), 0, j * (cX - differenceX), row * (cY - differenceY));
                        }

                    if (invoke)
                        Invoke((MethodInvoker)delegate () { map.Refresh(); });

                    invoke = false;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            catch { }
        }

        private void PainAllCalls()
        {
            using (var g = Graphics.FromImage(bmpPaTiles))
            {
                if (cX > cY)
                    differenceX = cX - cY;
                else
                    differenceY = cY - cX;

                g.Clear(Color.FromArgb(53, 129, 227));
                for (int i = 0; i < row; i++)
                    for (int j = 0; j < col; j++)
                    {
                        saveMap[i, j] = Convert.ToInt32(chooseTile.Tag);
                        g.DrawImage(chooseTile.Image, j * (cX - differenceX), i * (cX - differenceY), cX - differenceX, cY - differenceY);
                    }

                this.Invoke((MethodInvoker)delegate () { map.Refresh(); });
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
