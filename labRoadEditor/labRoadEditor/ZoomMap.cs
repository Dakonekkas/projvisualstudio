﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace LabRoadEditor
{
    class ZoomMap
    {
        
    }
}

#region

#endregion

namespace System.Drawing.OctavalentExtensions
{
    public static class BitmapExtensions
    {
        public static int rise = 200;
        public static int drop = -200;
        // переписать 
        public static Bitmap ScaleByPercent(this Bitmap imgPhoto, int Percent)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;

            var destWidth = (int)(sourceWidth - Percent);
            var destHeight = (int)(sourceHeight - Percent);

            var bmPhoto = new Bitmap(destWidth, destHeight,
                                     PixelFormat.Format24bppRgb);

            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                  imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                              new Rectangle(0, 0, destWidth, destHeight),
                              new Rectangle(0, 0, sourceWidth, sourceHeight),
                              GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
    }
}

