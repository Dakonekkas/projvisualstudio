﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabRoadEditor
{
    class CropTile
    {
        int imgY = 16;
        int imgX = 22;

        int indexW = 3;

        public List<Rectangle> tileList = new List<Rectangle>() {
            new Rectangle(3, 3, 64, 64), 
            new Rectangle(69, 3, 64, 64), 
            new Rectangle(135, 3, 64, 64), 
            new Rectangle(3, 69, 64, 64),
            new Rectangle(69, 69, 64, 64),
            new Rectangle(135, 69, 64, 64),
            new Rectangle(3, 135, 64, 64), 
            new Rectangle(69, 135, 64, 64), 
            new Rectangle(135, 135, 64, 64),

            new Rectangle(333, 3, 64, 64), // 0.5
            new Rectangle(399, 3, 64, 64), // 0.6
            new Rectangle(465, 3, 64, 64), // 0.7
            new Rectangle(333, 69, 64, 64),
            new Rectangle(399, 69, 64, 64),
            new Rectangle(465, 69, 64, 64),
            new Rectangle(333, 135, 64, 64), // 0.5
            new Rectangle(399, 135, 64, 64), // 0.6
            new Rectangle(465, 135, 64, 64),
            
            new Rectangle(3, 201, 64, 64), // 0.0
            new Rectangle(69, 201, 64, 64), // 0.1
            new Rectangle(135, 201, 64, 64), // 0.2
            new Rectangle(3, 267, 64, 64), // 0.0
            new Rectangle(69, 267, 64, 64), // 0.1
            new Rectangle(135, 267, 64, 64), // 0.2
            new Rectangle(3, 333, 64, 64),
            new Rectangle(69, 333, 64, 64), // 0.1
            new Rectangle(135, 333, 64, 64), // 0.2

            new Rectangle(201, 3, 64, 64),
            new Rectangle(267, 3, 64, 64), // 0.4
            new Rectangle(399, 201, 64, 64),
            new Rectangle(201, 69, 64, 64), // 0.3
            new Rectangle(267, 69, 64, 64), // 0.4
            new Rectangle(333, 267, 64, 64),
            
            
            
            new Rectangle(201, 135, 64, 64), // 0.3
            new Rectangle(267, 135, 64, 64), // 0.4
            new Rectangle(399, 267, 64, 64), // 0.6
            new Rectangle(201, 201, 64, 64), // 0.3
            new Rectangle(267, 201, 64, 64), // 0.4
            new Rectangle(465, 267, 64, 64), // 0.7



            new Rectangle(201, 267, 64, 64),
            new Rectangle(267, 267, 64, 64),
            new Rectangle(465, 333, 64, 64),
            new Rectangle(201, 333, 64, 64),
            new Rectangle(267, 333, 64, 64),

             new Rectangle(333, 201, 64, 64),
            new Rectangle(465, 201, 64, 64),
            new Rectangle(333, 333, 64, 64), 
            new Rectangle(399, 333, 64, 64), 
            
        };
    }
}
