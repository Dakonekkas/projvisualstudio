﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabRoadEditor
{
    public partial class ModeDialog : Form
    {
        private int curRow;
        private int curCol;

        public ModeDialog(int row, int col)
        {
            InitializeComponent();

            ChooseRow.Value = row;
            ChooseCol.Value = col;

            buOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            buCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public int getRow()
        {
            curRow = Convert.ToInt32(ChooseRow.Value);
            return curRow;
        }

        public int getCol()
        {
            curCol = Convert.ToInt32(ChooseCol.Value);
            return curCol;
        }
    }
}
