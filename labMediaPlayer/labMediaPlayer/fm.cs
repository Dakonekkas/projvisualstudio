﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace labMediaPlayer
{
    public partial class fm : Form
    {
        private MediaPlayer mediaPlayer = new MediaPlayer();
        public fm()
        {
            InitializeComponent();

            mediaPlayer.Open(new Uri(@"D:\Study\projVisualStudio\labMediaPlayer\Le_Castle_Vania_-_John_Wick_Mode.mp3"));

            buPlay.Click += (s, e) => mediaPlayer.Play();
            buPause.Click += (s, e) => mediaPlayer.Pause();
            buStop.Click += (s, e) => mediaPlayer.Stop();

            trVolume.Maximum = 100;
            trVolume.Value = (int)Math.Round(mediaPlayer.Volume * 100);
            trVolume.ValueChanged += (s, e) => mediaPlayer.Volume = trVolume.Value / 100.0;
        }
    }
}
