﻿namespace labMediaPlayer
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buPlay = new System.Windows.Forms.Button();
            this.buPause = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.trVolume = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.trVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // buPlay
            // 
            this.buPlay.Location = new System.Drawing.Point(27, 21);
            this.buPlay.Name = "buPlay";
            this.buPlay.Size = new System.Drawing.Size(92, 37);
            this.buPlay.TabIndex = 0;
            this.buPlay.Text = "Play";
            this.buPlay.UseVisualStyleBackColor = true;
            // 
            // buPause
            // 
            this.buPause.Location = new System.Drawing.Point(125, 21);
            this.buPause.Name = "buPause";
            this.buPause.Size = new System.Drawing.Size(89, 37);
            this.buPause.TabIndex = 1;
            this.buPause.Text = "Pause";
            this.buPause.UseVisualStyleBackColor = true;
            // 
            // buStop
            // 
            this.buStop.Location = new System.Drawing.Point(220, 21);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(92, 37);
            this.buStop.TabIndex = 2;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Volume";
            // 
            // trVolume
            // 
            this.trVolume.Location = new System.Drawing.Point(79, 64);
            this.trVolume.Name = "trVolume";
            this.trVolume.Size = new System.Drawing.Size(233, 56);
            this.trVolume.TabIndex = 4;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 109);
            this.Controls.Add(this.trVolume);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buPause);
            this.Controls.Add(this.buPlay);
            this.Name = "fm";
            this.Text = "labMediaPlayer";
            ((System.ComponentModel.ISupportInitialize)(this.trVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buPlay;
        private System.Windows.Forms.Button buPause;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trVolume;
    }
}

