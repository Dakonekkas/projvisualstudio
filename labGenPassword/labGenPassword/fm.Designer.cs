﻿namespace labGenPassword
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edPassword = new System.Windows.Forms.TextBox();
            this.buPassword = new System.Windows.Forms.Button();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckNumber = new System.Windows.Forms.CheckBox();
            this.ckSpec = new System.Windows.Forms.CheckBox();
            this.edLenght = new System.Windows.Forms.NumericUpDown();
            this.laLenght = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).BeginInit();
            this.SuspendLayout();
            // 
            // edPassword
            // 
            this.edPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edPassword.Location = new System.Drawing.Point(12, 12);
            this.edPassword.Name = "edPassword";
            this.edPassword.ReadOnly = true;
            this.edPassword.Size = new System.Drawing.Size(560, 83);
            this.edPassword.TabIndex = 0;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buPassword
            // 
            this.buPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buPassword.Location = new System.Drawing.Point(40, 102);
            this.buPassword.Name = "buPassword";
            this.buPassword.Size = new System.Drawing.Size(506, 55);
            this.buPassword.TabIndex = 1;
            this.buPassword.Text = "Сгенерировать";
            this.buPassword.UseVisualStyleBackColor = true;
            // 
            // ckLower
            // 
            this.ckLower.AutoSize = true;
            this.ckLower.Location = new System.Drawing.Point(12, 164);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(217, 21);
            this.ckLower.TabIndex = 2;
            this.ckLower.Text = "Символы в нижнем регистре";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.AutoSize = true;
            this.ckUpper.Location = new System.Drawing.Point(12, 192);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(221, 21);
            this.ckUpper.TabIndex = 3;
            this.ckUpper.Text = "Смоволы в верхнем регистре";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckNumber
            // 
            this.ckNumber.AutoSize = true;
            this.ckNumber.Location = new System.Drawing.Point(12, 220);
            this.ckNumber.Name = "ckNumber";
            this.ckNumber.Size = new System.Drawing.Size(78, 21);
            this.ckNumber.TabIndex = 4;
            this.ckNumber.Text = "Цифры";
            this.ckNumber.UseVisualStyleBackColor = true;
            // 
            // ckSpec
            // 
            this.ckSpec.AutoSize = true;
            this.ckSpec.Location = new System.Drawing.Point(12, 248);
            this.ckSpec.Name = "ckSpec";
            this.ckSpec.Size = new System.Drawing.Size(181, 21);
            this.ckSpec.TabIndex = 5;
            this.ckSpec.Text = "Специальные символы";
            this.ckSpec.UseVisualStyleBackColor = true;
            // 
            // edLenght
            // 
            this.edLenght.Location = new System.Drawing.Point(130, 273);
            this.edLenght.Name = "edLenght";
            this.edLenght.Size = new System.Drawing.Size(120, 22);
            this.edLenght.TabIndex = 6;
            this.edLenght.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // laLenght
            // 
            this.laLenght.AutoSize = true;
            this.laLenght.Location = new System.Drawing.Point(13, 275);
            this.laLenght.Name = "laLenght";
            this.laLenght.Size = new System.Drawing.Size(111, 17);
            this.laLenght.TabIndex = 7;
            this.laLenght.Text = "Длинна пароля";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 348);
            this.Controls.Add(this.laLenght);
            this.Controls.Add(this.edLenght);
            this.Controls.Add(this.ckSpec);
            this.Controls.Add(this.ckNumber);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buPassword);
            this.Controls.Add(this.edPassword);
            this.MinimumSize = new System.Drawing.Size(602, 395);
            this.Name = "fm";
            this.Text = "labGenPassword";
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Button buPassword;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckNumber;
        private System.Windows.Forms.CheckBox ckSpec;
        private System.Windows.Forms.NumericUpDown edLenght;
        private System.Windows.Forms.Label laLenght;
    }
}

