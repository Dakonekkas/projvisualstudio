﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labTrainerAccount
{
    public partial class fm : Form
    {
        private Games g;
        public fm()
        {
            InitializeComponent();
            //
            g = new Games();
            g.Change += Event_Change;
            g.DoReset();
            //
            buYes.Click += (sender, e) => g.DoAnswer(true);
            buNo.Click += (sender, e) => g.DoAnswer(false);
        }

        private void Event_Change(object sender, EventArgs e)
        {
            laCorrect.Text = String.Format("Верно = {0}", g.CountCorrect.ToString());
            laWrong.Text = String.Format("Неверно = {0}", g.CountWrong.ToString());
            labCode.Text = g.CodeText;
        }
    }
}
