﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPaint
{
    public partial class fm : Form
    {
        private bool isPressed;
        private bool line = false;
        private bool krest = false;
        private bool krug = false;
        private Bitmap b;

        public fm()
        {
            InitializeComponent();

            b = new Bitmap(paimage.Width, paimage.Height);
            paimage.MouseDown += paimage_MouseDown;
            paimage.MouseUp += paimage_MouseUp;
            paimage.MouseMove += paimage_MouseMove;
            paimage.Paint += paimage_Paint;
            //добавить функционал paint
            //изменения формы

            this.SizeChanged += (s, e) =>
            {
                int X = ((Form)s).Width;
                int Y = ((Form)s).Height;
                b = new Bitmap(paimage.Width, paimage.Height);
            };
        }

        private void paimage_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point(0, 0));
        }

        public void paimage_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isPressed) return;
            if (line == true)
            {
                var g = Graphics.FromImage(b);
                g.DrawLine(new Pen(Color.Black, 1), e.X + 10, e.Y + 10, e.X - 10, e.Y - 10);
                g.Dispose();
                paimage.CreateGraphics().DrawImage(b, new Point(0, 0));
            }
            else if (krest == true)
            {
                var g = Graphics.FromImage(b);
                g.DrawLine(new Pen(Color.Red, 5), e.X - 10, e.Y - 10, e.X + 10, e.Y + 10);
                g.DrawLine(new Pen(Color.Red, 5), e.X - 10, e.Y + 10, e.X + 10, e.Y - 10);
                g.Dispose();
                paimage.CreateGraphics().DrawImage(b, new Point(0, 0));
            }

            else if (krug == true)
            {
                Pen pen = new Pen(Brushes.Red);
                Graphics krug = paimage.CreateGraphics();
                krug.DrawEllipse(new Pen(Color.Green, 1), e.X + 10, e.Y + 10, e.X - 10, e.Y - 10);
                krug.Dispose();
                paimage.CreateGraphics().DrawImage(b, new Point(0, 0));
            }
        }

        private void paimage_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false;
        }

        private void paimage_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                isPressed = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            line = true;
            krest = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            krest = true;
            line = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            krest = false;
            line = false;
            krug = true;
        }
    }
}
