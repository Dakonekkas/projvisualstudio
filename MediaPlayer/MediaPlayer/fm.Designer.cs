﻿namespace MediaPlayer
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Volume = new System.Windows.Forms.TrackBar();
            this.Level = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PlayListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скоростьвоспроизведенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Speed_05 = new System.Windows.Forms.ToolStripMenuItem();
            this.Speed_025 = new System.Windows.Forms.ToolStripMenuItem();
            this.Speed_075 = new System.Windows.Forms.ToolStripMenuItem();
            this.Speed_1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Speed_125 = new System.Windows.Forms.ToolStripMenuItem();
            this.Speed_175 = new System.Windows.Forms.ToolStripMenuItem();
            this.Speed_2 = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Playlist = new System.Windows.Forms.ListBox();
            this.OpenFiles = new System.Windows.Forms.Button();
            this.MediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Back = new System.Windows.Forms.Button();
            this.Next = new System.Windows.Forms.Button();
            this.Play = new System.Windows.Forms.Button();
            this.Pause = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BackTo_10 = new System.Windows.Forms.Button();
            this.Stop = new System.Windows.Forms.Button();
            this.ForwardTo_10 = new System.Windows.Forms.Button();
            this.PlayListBox = new System.Windows.Forms.Panel();
            this.TimeNow = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.AddFile = new System.Windows.Forms.Button();
            this.DeleteOneItem = new System.Windows.Forms.Button();
            this.PlayListClear = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.WatchWithStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.InfoStripButton = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.OK = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MediaPlayer)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PlayListBox.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // trackBar1
            // 
            this.trackBar1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1.Location = new System.Drawing.Point(0, 2);
            this.trackBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(952, 56);
            this.trackBar1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(3, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 22);
            this.label1.TabIndex = 6;
            this.label1.Text = "00:00";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(845, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 22);
            this.label2.TabIndex = 7;
            this.label2.Text = "00:00";
            // 
            // Volume
            // 
            this.Volume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Volume.Location = new System.Drawing.Point(429, 81);
            this.Volume.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Volume.Maximum = 100;
            this.Volume.Name = "Volume";
            this.Volume.Size = new System.Drawing.Size(195, 56);
            this.Volume.TabIndex = 9;
            this.Volume.Value = 50;
            // 
            // Level
            // 
            this.Level.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Level.AutoSize = true;
            this.Level.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Level.Location = new System.Drawing.Point(629, 90);
            this.Level.Name = "Level";
            this.Level.Size = new System.Drawing.Size(32, 22);
            this.Level.TabIndex = 10;
            this.Level.Text = "50";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenToolStripMenuItem,
            this.PlayListToolStripMenuItem,
            this.скоростьвоспроизведенияToolStripMenuItem,
            this.AboutMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(947, 28);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // OpenToolStripMenuItem
            // 
            this.OpenToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.OpenToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.OpenToolStripMenuItem.Image = global::MediaPlayer.Properties.Resources.OpenVideo;
            this.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem";
            this.OpenToolStripMenuItem.Size = new System.Drawing.Size(110, 24);
            this.OpenToolStripMenuItem.Text = "Открыть...";
            // 
            // PlayListToolStripMenuItem
            // 
            this.PlayListToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.PlayListToolStripMenuItem.Image = global::MediaPlayer.Properties.Resources.PlayList;
            this.PlayListToolStripMenuItem.Name = "PlayListToolStripMenuItem";
            this.PlayListToolStripMenuItem.Size = new System.Drawing.Size(222, 24);
            this.PlayListToolStripMenuItem.Text = "Список воспроизведения";
            // 
            // скоростьвоспроизведенияToolStripMenuItem
            // 
            this.скоростьвоспроизведенияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Speed_05,
            this.Speed_025,
            this.Speed_075,
            this.Speed_1,
            this.Speed_125,
            this.Speed_175,
            this.Speed_2});
            this.скоростьвоспроизведенияToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.скоростьвоспроизведенияToolStripMenuItem.Image = global::MediaPlayer.Properties.Resources.Speed;
            this.скоростьвоспроизведенияToolStripMenuItem.Name = "скоростьвоспроизведенияToolStripMenuItem";
            this.скоростьвоспроизведенияToolStripMenuItem.Size = new System.Drawing.Size(178, 24);
            this.скоростьвоспроизведенияToolStripMenuItem.Text = "Изменить скорость";
            this.скоростьвоспроизведенияToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // Speed_05
            // 
            this.Speed_05.Name = "Speed_05";
            this.Speed_05.Size = new System.Drawing.Size(156, 26);
            this.Speed_05.Text = "0.5";
            // 
            // Speed_025
            // 
            this.Speed_025.Name = "Speed_025";
            this.Speed_025.Size = new System.Drawing.Size(156, 26);
            this.Speed_025.Text = "0.25";
            // 
            // Speed_075
            // 
            this.Speed_075.Name = "Speed_075";
            this.Speed_075.Size = new System.Drawing.Size(156, 26);
            this.Speed_075.Text = "0.75";
            // 
            // Speed_1
            // 
            this.Speed_1.Name = "Speed_1";
            this.Speed_1.Size = new System.Drawing.Size(156, 26);
            this.Speed_1.Text = "Обычная";
            // 
            // Speed_125
            // 
            this.Speed_125.Name = "Speed_125";
            this.Speed_125.Size = new System.Drawing.Size(156, 26);
            this.Speed_125.Text = "1.25";
            // 
            // Speed_175
            // 
            this.Speed_175.Name = "Speed_175";
            this.Speed_175.Size = new System.Drawing.Size(156, 26);
            this.Speed_175.Text = "1.75";
            // 
            // Speed_2
            // 
            this.Speed_2.Name = "Speed_2";
            this.Speed_2.Size = new System.Drawing.Size(156, 26);
            this.Speed_2.Text = "2";
            // 
            // AboutMenuItem
            // 
            this.AboutMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.AboutMenuItem.Image = global::MediaPlayer.Properties.Resources.About;
            this.AboutMenuItem.Name = "AboutMenuItem";
            this.AboutMenuItem.Size = new System.Drawing.Size(138, 24);
            this.AboutMenuItem.Text = "О программе";
            // 
            // Playlist
            // 
            this.Playlist.BackColor = System.Drawing.Color.Black;
            this.Playlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Playlist.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Playlist.FormattingEnabled = true;
            this.Playlist.ItemHeight = 16;
            this.Playlist.Location = new System.Drawing.Point(1, 1);
            this.Playlist.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Playlist.Name = "Playlist";
            this.Playlist.Size = new System.Drawing.Size(269, 336);
            this.Playlist.TabIndex = 12;
            // 
            // OpenFiles
            // 
            this.OpenFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.OpenFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OpenFiles.Font = new System.Drawing.Font("Tempus Sans ITC", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpenFiles.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.OpenFiles.Location = new System.Drawing.Point(715, 81);
            this.OpenFiles.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OpenFiles.Name = "OpenFiles";
            this.OpenFiles.Size = new System.Drawing.Size(160, 39);
            this.OpenFiles.TabIndex = 14;
            this.OpenFiles.Text = "Открыть файл...";
            this.OpenFiles.UseVisualStyleBackColor = false;
            // 
            // MediaPlayer
            // 
            this.MediaPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MediaPlayer.Enabled = true;
            this.MediaPlayer.Location = new System.Drawing.Point(0, 0);
            this.MediaPlayer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MediaPlayer.Name = "MediaPlayer";
            this.MediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("MediaPlayer.OcxState")));
            this.MediaPlayer.Size = new System.Drawing.Size(947, 580);
            this.MediaPlayer.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(683, 90);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 22);
            this.label3.TabIndex = 18;
            this.label3.Text = "%";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.Back);
            this.panel1.Controls.Add(this.OpenFiles);
            this.panel1.Controls.Add(this.Next);
            this.panel1.Controls.Add(this.Level);
            this.panel1.Controls.Add(this.Play);
            this.panel1.Controls.Add(this.Volume);
            this.panel1.Controls.Add(this.Pause);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.BackTo_10);
            this.panel1.Controls.Add(this.Stop);
            this.panel1.Controls.Add(this.ForwardTo_10);
            this.panel1.Controls.Add(this.trackBar1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 457);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(947, 123);
            this.panel1.TabIndex = 19;
            // 
            // Back
            // 
            this.Back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Back.BackgroundImage = global::MediaPlayer.Properties.Resources.Back;
            this.Back.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Back.Location = new System.Drawing.Point(51, 80);
            this.Back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(39, 39);
            this.Back.TabIndex = 17;
            this.Back.UseVisualStyleBackColor = true;
            // 
            // Next
            // 
            this.Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Next.BackgroundImage = global::MediaPlayer.Properties.Resources.Next;
            this.Next.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Next.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Next.Location = new System.Drawing.Point(315, 80);
            this.Next.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(39, 39);
            this.Next.TabIndex = 16;
            this.Next.UseVisualStyleBackColor = true;
            // 
            // Play
            // 
            this.Play.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Play.BackgroundImage = global::MediaPlayer.Properties.Resources.Play;
            this.Play.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Play.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Play.Location = new System.Drawing.Point(140, 80);
            this.Play.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Play.Name = "Play";
            this.Play.Size = new System.Drawing.Size(39, 39);
            this.Play.TabIndex = 0;
            this.Play.UseVisualStyleBackColor = true;
            // 
            // Pause
            // 
            this.Pause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Pause.BackgroundImage = global::MediaPlayer.Properties.Resources.Pause;
            this.Pause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Pause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pause.Location = new System.Drawing.Point(184, 79);
            this.Pause.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Pause.Name = "Pause";
            this.Pause.Size = new System.Drawing.Size(39, 39);
            this.Pause.TabIndex = 1;
            this.Pause.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Image = global::MediaPlayer.Properties.Resources.Speakers;
            this.pictureBox1.Location = new System.Drawing.Point(381, 79);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(43, 39);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // BackTo_10
            // 
            this.BackTo_10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BackTo_10.BackgroundImage = global::MediaPlayer.Properties.Resources.rewind1;
            this.BackTo_10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.BackTo_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BackTo_10.Location = new System.Drawing.Point(96, 80);
            this.BackTo_10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BackTo_10.Name = "BackTo_10";
            this.BackTo_10.Size = new System.Drawing.Size(39, 39);
            this.BackTo_10.TabIndex = 2;
            this.BackTo_10.UseVisualStyleBackColor = true;
            // 
            // Stop
            // 
            this.Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Stop.BackgroundImage = global::MediaPlayer.Properties.Resources.Stop;
            this.Stop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Stop.Location = new System.Drawing.Point(227, 79);
            this.Stop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Stop.Name = "Stop";
            this.Stop.Size = new System.Drawing.Size(39, 39);
            this.Stop.TabIndex = 13;
            this.Stop.UseVisualStyleBackColor = true;
            // 
            // ForwardTo_10
            // 
            this.ForwardTo_10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ForwardTo_10.BackgroundImage = global::MediaPlayer.Properties.Resources.rewind2;
            this.ForwardTo_10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ForwardTo_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ForwardTo_10.Location = new System.Drawing.Point(271, 79);
            this.ForwardTo_10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ForwardTo_10.Name = "ForwardTo_10";
            this.ForwardTo_10.Size = new System.Drawing.Size(39, 39);
            this.ForwardTo_10.TabIndex = 3;
            this.ForwardTo_10.UseVisualStyleBackColor = true;
            // 
            // PlayListBox
            // 
            this.PlayListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PlayListBox.Controls.Add(this.TimeNow);
            this.PlayListBox.Controls.Add(this.label9);
            this.PlayListBox.Controls.Add(this.AddFile);
            this.PlayListBox.Controls.Add(this.DeleteOneItem);
            this.PlayListBox.Controls.Add(this.PlayListClear);
            this.PlayListBox.Controls.Add(this.Playlist);
            this.PlayListBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.PlayListBox.Location = new System.Drawing.Point(680, 28);
            this.PlayListBox.Margin = new System.Windows.Forms.Padding(4);
            this.PlayListBox.Name = "PlayListBox";
            this.PlayListBox.Size = new System.Drawing.Size(267, 429);
            this.PlayListBox.TabIndex = 20;
            // 
            // TimeNow
            // 
            this.TimeNow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TimeNow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TimeNow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.TimeNow.Location = new System.Drawing.Point(115, 349);
            this.TimeNow.Multiline = true;
            this.TimeNow.Name = "TimeNow";
            this.TimeNow.Size = new System.Drawing.Size(140, 22);
            this.TimeNow.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(4, 351);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "Текущая дата:";
            // 
            // AddFile
            // 
            this.AddFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddFile.BackgroundImage = global::MediaPlayer.Properties.Resources.AddFile;
            this.AddFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AddFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddFile.Location = new System.Drawing.Point(145, 383);
            this.AddFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.AddFile.Name = "AddFile";
            this.AddFile.Size = new System.Drawing.Size(41, 41);
            this.AddFile.TabIndex = 21;
            this.AddFile.UseVisualStyleBackColor = true;
            // 
            // DeleteOneItem
            // 
            this.DeleteOneItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteOneItem.BackgroundImage = global::MediaPlayer.Properties.Resources.Delete1;
            this.DeleteOneItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.DeleteOneItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteOneItem.Location = new System.Drawing.Point(80, 383);
            this.DeleteOneItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DeleteOneItem.Name = "DeleteOneItem";
            this.DeleteOneItem.Size = new System.Drawing.Size(41, 41);
            this.DeleteOneItem.TabIndex = 20;
            this.DeleteOneItem.UseVisualStyleBackColor = true;
            // 
            // PlayListClear
            // 
            this.PlayListClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PlayListClear.BackgroundImage = global::MediaPlayer.Properties.Resources.Trash;
            this.PlayListClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PlayListClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayListClear.Location = new System.Drawing.Point(4, 383);
            this.PlayListClear.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PlayListClear.Name = "PlayListClear";
            this.PlayListClear.Size = new System.Drawing.Size(41, 41);
            this.PlayListClear.TabIndex = 19;
            this.PlayListClear.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WatchWithStripButton1,
            this.InfoStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(680, 27);
            this.toolStrip1.TabIndex = 21;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // WatchWithStripButton1
            // 
            this.WatchWithStripButton1.Image = global::MediaPlayer.Properties.Resources.Time;
            this.WatchWithStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WatchWithStripButton1.Name = "WatchWithStripButton1";
            this.WatchWithStripButton1.Size = new System.Drawing.Size(122, 24);
            this.WatchWithStripButton1.Text = "Смотреть с....";
            // 
            // InfoStripButton
            // 
            this.InfoStripButton.Image = global::MediaPlayer.Properties.Resources.DataBase;
            this.InfoStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.InfoStripButton.Name = "InfoStripButton";
            this.InfoStripButton.Size = new System.Drawing.Size(132, 24);
            this.InfoStripButton.Text = "Инфо о файле";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.OK);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Location = new System.Drawing.Point(0, 68);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(308, 122);
            this.panel2.TabIndex = 22;
            // 
            // OK
            // 
            this.OK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OK.Font = new System.Drawing.Font("Tempus Sans ITC", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.OK.Location = new System.Drawing.Point(195, 92);
            this.OK.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(111, 30);
            this.OK.TabIndex = 19;
            this.OK.Text = "Применить";
            this.OK.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(239, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 22);
            this.label8.TabIndex = 6;
            this.label8.Text = "сек.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(99, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 22);
            this.label7.TabIndex = 5;
            this.label7.Text = "сек.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Font = new System.Drawing.Font("Tempus Sans ITC", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(280, 18);
            this.label6.TabIndex = 4;
            this.label6.Text = "Введите промежуток времени в сек.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(144, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 22);
            this.label5.TabIndex = 3;
            this.label5.Text = "до";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tempus Sans ITC", 10.2F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(15, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 22);
            this.label4.TabIndex = 2;
            this.label4.Text = "с";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(183, 50);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(49, 27);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "20";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(41, 50);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(51, 27);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "6";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(947, 580);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.PlayListBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.MediaPlayer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "fm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MediaPlayer";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Volume)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MediaPlayer)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PlayListBox.ResumeLayout(false);
            this.PlayListBox.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Play;
        private System.Windows.Forms.Button Pause;
        private System.Windows.Forms.Button BackTo_10;
        private System.Windows.Forms.Button ForwardTo_10;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TrackBar Volume;
        private System.Windows.Forms.Label Level;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem OpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PlayListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скоростьвоспроизведенияToolStripMenuItem;
        private System.Windows.Forms.ListBox Playlist;
        private System.Windows.Forms.Button Stop;
        private System.Windows.Forms.Button OpenFiles;
        private AxWMPLib.AxWindowsMediaPlayer MediaPlayer;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PlayListBox;
        private System.Windows.Forms.Button PlayListClear;
        private System.Windows.Forms.Button DeleteOneItem;
        private System.Windows.Forms.Button AddFile;
        private System.Windows.Forms.ToolStripMenuItem AboutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Speed_05;
        private System.Windows.Forms.ToolStripMenuItem Speed_025;
        private System.Windows.Forms.ToolStripMenuItem Speed_075;
        private System.Windows.Forms.ToolStripMenuItem Speed_1;
        private System.Windows.Forms.ToolStripMenuItem Speed_125;
        private System.Windows.Forms.ToolStripMenuItem Speed_175;
        private System.Windows.Forms.ToolStripMenuItem Speed_2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton WatchWithStripButton1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.ToolStripButton InfoStripButton;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TimeNow;
    }
}

