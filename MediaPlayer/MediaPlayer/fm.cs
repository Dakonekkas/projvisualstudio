﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using System.Windows.Forms;

namespace MediaPlayer
{
    public partial class fm : Form
    {
        OpenFileDialog openFile = new OpenFileDialog();
        string[] paths = new string [0];
        string[] files = new string [0];
        Timer timer = new Timer();
        string namefile = "";
        private readonly SQLiteConnection db;
        string dateTime = DateTime.Now.ToString();

        public fm()
        {
            InitializeComponent();
            //
            db = new SQLiteConnection("VideoInfo.db");
            db.CreateTable<Info>();
            //
            openFile.Filter = "( *.mp4)|*.mp4";
            //
            PlayListBox.Visible = false;
            panel2.Visible = false;
            //
            OpenFiles.Click += OpenFiles_Click;
            Playlist.SelectedIndexChanged += Playlist_SelectedIndexChanged;
            Play.Click += Play_Click;
            Pause.Click += Pause_Click;
            Stop.Click += Stop_Click;
            BackTo_10.Click += BackTo_10_Click;
            ForwardTo_10.Click += ForwardTo_10_Click;
            Next.Click += Next_Click;
            Back.Click += Back_Click;
            Volume.Scroll += Volume_Scroll;
            trackBar1.MouseUp += TrackBar1_MouseUp;
            trackBar1.MouseDown += (s, e) => timer.Stop();
            MediaPlayer.PlayStateChange += MediaPlayer_PlayStateChange;
            OpenToolStripMenuItem.Click += OpenToolStripMenuItem_Click;
            PlayListToolStripMenuItem.Click += PlayListToolStripMenuItem_Click;
            PlayListClear.Click += (s, e) => Playlist.Items.Clear();
            AddFile.Click += AddFile_Click;
            DeleteOneItem.Click += DeleteOneItem_Click;
            AboutMenuItem.Click += AboutMenuItem_Click;
            OK.Click += OK_Click;
            WatchWithStripButton1.Click += WatchWithStripButton1_Click;
            InfoStripButton.Click += InfoStripButton_Click;
            //
            timer.Interval = 100;
            timer.Tick += Timer_Tick;
            //
            Volume.Value = 50;
            //
            Speed_05.Click += (s, e) => MediaPlayer.settings.rate = 0.5;
            Speed_025.Click += (s, e) => MediaPlayer.settings.rate = 0.25;
            Speed_075.Click += (s, e) => MediaPlayer.settings.rate = 0.75;
            Speed_1.Click += (s, e) => MediaPlayer.settings.rate = 1;
            Speed_125.Click += (s, e) => MediaPlayer.settings.rate = 1.25;
            Speed_175.Click += (s, e) => MediaPlayer.settings.rate = 1.75;
            Speed_2.Click += (s, e) => MediaPlayer.settings.rate = 2;

            TimeNow.Text = dateTime;
        }

        private void InfoStripButton_Click(object sender, EventArgs e)
        {
            string pathes = db.ExecuteScalar<string>($"SELECT paths FROM `Info` WHERE files = \"{namefile}\"");
            string number = db.ExecuteScalar<string>($"SELECT nod FROM `Info` WHERE files = \"{namefile}\"");
            int longes = db.ExecuteScalar<int>($"SELECT longs FROM `Info` WHERE files = \"{namefile}\"");
            //
                MessageBox.Show($"Имя: {namefile} {Environment.NewLine}" +
                $"{Environment.NewLine}Путь: {pathes} " +
                $"{Environment.NewLine}Продолжительность: {longes} сек." +
                $"{Environment.NewLine}Количество открытий: {number}",
                "Media Player", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void WatchWithStripButton1_Click(object sender, EventArgs e)
        {
            if (panel2.Visible == true)
            {
                panel2.Visible = false;
            }
            else
            {
                panel2.Visible = true;
            }
        }

        private void OK_Click(object sender, EventArgs e)
        {
            MediaPlayer.Ctlcontrols.currentPosition = Convert.ToInt32(textBox1.Text);
            Timer WatchWith = new Timer();
            WatchWith.Interval = (Convert.ToInt32(textBox2.Text) * 1000 - Convert.ToInt32(textBox1.Text) * 1000) + 100;
            WatchWith.Tick += WatchWith_Tick;
            WatchWith.Start();
        }

        private void WatchWith_Tick(object sender, EventArgs e)
        {
            MediaPlayer.Ctlcontrols.pause();
            MessageBox.Show($"Данный интервал просмотрен", "Media Player", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void AboutMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Media Player{Environment.NewLine}Версия 1.0 - Альфа" +
            $"{Environment.NewLine}Дата: June 30 2020 09:17:50" +
            $"{Environment.NewLine}Авторское право © 2020 Московский Политех",
            "Media Player", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void DeleteOneItem_Click(object sender, EventArgs e)
        {
            string[] pathclone = paths;
            Array.Resize(ref paths, 0);
            string[] nameclone = files;
            Array.Resize(ref files, 0);
            for (int i = 0; i < pathclone.Length; i++)
            {
                if (i != Playlist.SelectedIndex)
                {
                    Array.Resize(ref files, files.Length + 1);
                    files[files.Length - 1] = nameclone[i];
                    Array.Resize(ref paths, paths.Length + 1);
                    paths[paths.Length - 1] = pathclone[i];
                }
            }
            Playlist.Items.Clear();
            Playlist.Items.AddRange(files);
        }

        private void AddFile_Click(object sender, EventArgs e)
        {
            OpenFile();
        }
        
        private void PlayListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PlayListBox.Visible == true)
            {
                PlayListBox.Visible = false;
            }
            else
            {
                PlayListBox.Visible = true;
            }
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            label1.Text = MediaPlayer.Ctlcontrols.currentPositionString;
            label2.Text = MediaPlayer.Ctlcontrols.currentItem.durationString.ToString();
            if(MediaPlayer.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                trackBar1.Value = (int)MediaPlayer.Ctlcontrols.currentPosition;
            }
        }

        private void MediaPlayer_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (MediaPlayer.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                trackBar1.Maximum = (int)MediaPlayer.Ctlcontrols.currentItem.duration;                
                timer.Start();
                //
                int nf = db.ExecuteScalar<int>($"SELECT Count(*) FROM `Info` WHERE files = \"{namefile}\"");
                if (nf == 0)
                {                    
                    db.Execute($"INSERT INTO `Info` (`files`, `paths`, `nod`, `longs`) VALUES (\"{namefile}\",\"{MediaPlayer.URL}\", 0, \"{MediaPlayer.Ctlcontrols.currentItem.duration}\")");
                }
                else
                {
                    int OldNod = db.ExecuteScalar<int>($"SELECT nod FROM `Info` WHERE files = \"{namefile}\"");
                    db.Execute($"UPDATE Info SET nod = \"{OldNod + 1}\" WHERE files = \"{namefile}\"");
                }
            }
            else if (MediaPlayer.playState==WMPLib.WMPPlayState.wmppsPaused)
            {
                timer.Stop();
            }
            else if(MediaPlayer.playState == WMPLib.WMPPlayState.wmppsStopped)
            {
                timer.Stop();
                trackBar1.Value = 0;
            }

            if (MediaPlayer.playState == WMPLib.WMPPlayState.wmppsMediaEnded)
            {
                if (Playlist.SelectedIndex < Playlist.Items.Count - 1)
                {
                    Playlist.SelectedIndex = Playlist.SelectedIndex + 1;
                }
            }
        }

        private void TrackBar1_MouseUp(object sender, MouseEventArgs e)
        {
            MediaPlayer.Ctlcontrols.currentPosition = trackBar1.Value;
            timer.Start();
        }

        private void Volume_Scroll(object sender, EventArgs e)
        {
            MediaPlayer.settings.volume = Volume.Value;
            Level.Text = Volume.Value.ToString();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            if (Playlist.SelectedIndex > 0)
            {
                Playlist.SelectedIndex = Playlist.SelectedIndex - 1;
            }
        }

        private void Next_Click(object sender, EventArgs e)
        {
            if (Playlist.SelectedIndex < Playlist.Items.Count - 1)
            {
                Playlist.SelectedIndex = Playlist.SelectedIndex + 1;
            }
        }

        private void ForwardTo_10_Click(object sender, EventArgs e)
        {
            MediaPlayer.Ctlcontrols.currentPosition += 10;
            trackBar1.Value = (int)MediaPlayer.Ctlcontrols.currentPosition;
            label1.Text = $"{(int)MediaPlayer.Ctlcontrols.currentPosition / 60}:{(int)MediaPlayer.Ctlcontrols.currentPosition - (((int)MediaPlayer.Ctlcontrols.currentPosition / 60) * 60)}";
        }

        private void BackTo_10_Click(object sender, EventArgs e)
        {
            MediaPlayer.Ctlcontrols.currentPosition -= 10;
            trackBar1.Value = (int)MediaPlayer.Ctlcontrols.currentPosition;
            label1.Text = $"{(int)MediaPlayer.Ctlcontrols.currentPosition / 60}:{(int)MediaPlayer.Ctlcontrols.currentPosition - (((int)MediaPlayer.Ctlcontrols.currentPosition / 60) * 60)}";
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            MediaPlayer.Ctlcontrols.stop();
        }

        private void Pause_Click(object sender, EventArgs e)
        {
            MediaPlayer.Ctlcontrols.pause();
        }

        private void Play_Click(object sender, EventArgs e)
        {
            MediaPlayer.Ctlcontrols.play();
        }

        private void Playlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            MediaPlayer.URL = paths[Playlist.SelectedIndex];
            namefile = Playlist.SelectedItem.ToString();
        }

        private void OpenFiles_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void OpenFile()
        {
            openFile.Multiselect = true;
            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int CountFiles = files.Length;
                int CountPaths = paths.Length;

                Array.Resize(ref paths, paths.Length + openFile.FileNames.Length);
                for (int i = 0; i < openFile.FileNames.Length; i++)
                {
                    paths[CountPaths] = openFile.FileNames[i];
                    ++CountPaths;
                }

                Array.Resize(ref files, files.Length + openFile.SafeFileNames.Length);
                for (int i = 0; i < openFile.SafeFileNames.Length; i++)
                {
                    files[CountFiles] = openFile.SafeFileNames[i];
                    ++CountFiles;
                }
                Playlist.Items.Clear();
                for (int i = 0; i < files.Length; i++)
                {
                    Playlist.Items.Add(files[i]);

                }
            }
        }

        private class Info
        {
            [PrimaryKey, AutoIncrement]
            public int id { get; set; }
            public string files { get; set; }
            public string paths { get; set; }
            public int longs { get; set; }
            public int nod { get; set; }
        }
    }
}
